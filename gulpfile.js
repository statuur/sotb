'use strict';

var gulp        		= require('gulp');
var browserSync 		= require('browser-sync').create();
//var sass 				= require('gulp-sass');
var gettext 			= require('gulp-angular-gettext');
var reload      		= browserSync.reload;
//var sassBuild 			= require('ionic-gulp-sass-build');
//var minifyjs 			= require('gulp-js-minify');
var concat 				= require('gulp-concat');
//var sourcemaps 			= require('gulp-sourcemaps');
//var ngAnnotate 			= require('gulp-ng-annotate');
//var plumber 			= require('gulp-plumber');
//console.log(ngAnnotate );


var jsFiles = 'js/*.js',
    jsDest = 'dist';


gulp.task('app', function() {
    return gulp.src(['js/*.js'])
	    .pipe(plumber())
			.pipe(concat('app.js', {newLine: ';'}))
			.pipe(ngAnnotate({add: true}))
	    .pipe(plumber.stop())
        .pipe(gulp.dest(jsDest));
});



gulp.task('pot', function () {
   return gulp.src(['templates/*.html', 'js/*.js', 'lib/**/*.js'])
        .pipe(gettext.extract('template.pot', {
            // options to pass to angular-gettext-tools... 
        }))
        .pipe(gulp.dest('po/'));
});
 
gulp.task('translations', function () {
   return gulp.src('po/**/*.po')
        .pipe(gettext.compile({
            // options to pass to angular-gettext-tools... 
            format: 'json'
        }))
        .pipe(gulp.dest('translations/'));
});

// Watch scss AND html files, doing different things with each.
gulp.task('serve', function () {

    // Serve files from the root of this project
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch("*.html").on("change", reload);
    gulp.watch("./templates/*.html").on("change", reload);
    gulp.watch("./js/*.js").on("change", reload);
    gulp.watch("./lib/**/*.js").on("change", reload);
    //gulp.watch("./sass/*.scss").on("change", reload);
    gulp.watch("./css/*.css").on("change", reload);
});

gulp.task('default', ['serve']);