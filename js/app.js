"use strict";
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
//'ionic.native', 

var app= angular.module('app', ['ngAnimate', 'ionic', 'app.controllers', 'app.routes', 'app.directives','app.services', 'ngFileUpload', 'gettext', 'ngMap'])//'ngColorThief'
//.config(function($ionicConfigProvider, $sceDelegateProvider){})
.run(function($ionicPlatform, $rootScope, $ionicLoading, $window, $state, Storage, gettextCatalog) {
$rootScope.isMobile = ionic.Platform.isIOS() || ionic.Platform.isAndroid();
  
gettextCatalog.setStrings('de_DE', {"Aanpassen:":"Anpassen:","Beginnen":"Starten","Foto":"Foto","Ga bij de hommelkast zitten en tel de hommels die in- en uitvliegen. Kijk goed of ze ook pollen aan hun pootjes hebben.":"Setz dich auf die Hummel und zähle die Hummeln, die rein und raus fliegen. Stellen Sie sicher, dass sie auch Pollen an ihren Beinen haben.","Ga naar buiten en zoek een plek waar je hommels kunt tegenkomen. Probeer in een kwartier tijd zoveel mogelijk hommels op te sporen en te fotograferen. Hoeveel kan jij er vinden?":"Geh nach draußen und finde einen Ort, an dem du Hummeln begegnen kannst. Versuchen Sie, so viele Hummeln wie möglich in fünfzehn Minuten zu verfolgen und zu fotografieren. Wie viel kannst du finden?","Geteld":"Gezählt","Getelde hommels":"Zählhummeln","Helaas geen foto's":"Leider keine Bilder","Helaas...volgende week beter!":"Leider besser nächste Woche!","Het e-mail veld mag niet leeg zijn":"Das e-mail-feld darf nicht leer sein","Het e-mailadres is verkeerd geschreven":"Die e-mail-adresse wurde falsch geschrieben","Hommel zonder foto":"Hummeln ohne Foto","Hommeljacht":"Hummelschrank","Hommelkast":"Hummelschrank","Hommels zonder foto":"Hummeln ohne Foto","Inloggen":"Einloggen","Ja":"Ja","Je gaat nu terug naar het beginscherm.":"Sie kehren jetzt zum Startbildschirm zurück.","Je gegevens gaan op deze manier helaas verloren.":"Leider sind Ihre Daten auf diese Weise verloren.","Je hebt <b>{{totaaltijd}}</b> minuten geteld.\n\t      <br>Hieronder kun je nog aanpassen.":"Sie haben <b> {{totaaltiid}} </ b> Minuten gezählt. <br> Sie können unten noch anpassen.","Je kunt op dit moment niet inloggen omdat je niet online bent":"Sie können sich zu diesem Zeitpunkt nicht anmelden, da Sie nicht online sind","Kies hieronder het soort beplanting of bebouwing bij de kleuren. {{hasEmptyValues}}":"Wählen Sie unten die Art der Bepflanzung oder Gebäude mit den Farben. {{hasEmptyValues}}","Klaar!":"Fertig!","Kleur":"Farbe","Klik op de cijfers als je nog wilt aanpassen":"Klicken Sie auf die Zahlen, wenn Sie noch anpassen möchten","Klik op het nummer bij 'Geteld' en pas aan":"Klicken Sie auf die Nummer unter “Gezählt” und passen Sie sie an","Maak een foto van de kaart. Zorg ervoor dat de kaart zo groot mogelijk op de foto komt.":"Mach ein Foto von der Karte. Stellen Sie sicher, dass die Karte so groß wie möglich auf dem Foto ist.","Nee":"Nein","Niet online!":"Nicht online!","Om te kunnen opslaan moet je een werkende internetverbinding hebben. Loop naar een wifipunt of maak op een andere manier verbindng.":"Um zu speichern, müssen Sie eine funktionierende Internetverbindung haben. Gehe zu einem WLAN-Punkt oder verbinde dich auf andere Weise.","Omgevingsanalyse":"Umweltanalyse","Omgevingsanalyse:Stap 2":"Umweltanalyse: Schritt 2","Omgevingsanalyse:Stap {{stap}}":"Umweltanalyse: Schritt {{stap}}","Opslaan":"speichern","Per kaart die je hebt getekend hebt ga je onder anderen kijken hoe groot deel daarvan bloeiende planten waren.":"Für jede Karte, die Sie gezeichnet haben, werden Sie sehen, wie viele von ihnen blühende Pflanzen waren.","Stap":"Schritt","Stap 2":"Schritt 2","Swipe naar links bij 'Foto's' om te wissen.":"Streichen Sie bei “Fotos” nach links, um sie zu löschen.","Taal":"Sprache","Uitloggen":"Abmelden","Upload gegevens":"Daten hochladen","Uploaden Foto's":"Fotos hochladen","Volgende stap":"Nächster Schritt","WELKOM BIJ DE HOMMELAPP {{gebruiker.display_name}}":"WILLKOMMEN BEI DER HOMMELAPP {{user.display_name}}","Wat een jager ben jij, fantastisch!":"Was für ein Jäger bist du, fantastisch!","Weet je zeker dat je af wilt sluiten?":"Möchtest du wirklich schließen?","Welkom, klik hieronder om te beginnen.":"Willkommen, klicken Sie unten um zu beginnen.","Wissen":"Löschen","account aanmaken":"Oder erstelle ein Konto","alleen tellen<br>(hij was al weg)":"nur zählen,<br/>(er war schon weg)","analyse":"Analyse","annuleren":"abbrechen","foto maken":"foto machen","foto nemen":"foto machen","geweldig!<br>dank voor je hulp!":"großartig!<br>danke für deine hilfe!","het lukt niet om nu verbinding te maken":"Sie können jetzt keine Verbindung herstellen","hommeljacht":"hummelschrank","hommelkast":"hummelschrank","hommels met pollen":"hummeln mit pollen","hommels zonder pollen":"hummeln ohne pollen","instellingen":"einstellungen","je bent nu <b my-current-time=\"jagenOverzicht.html\">0:00</b> minuten bezig met jagen":"Sie sind jetzt <b my-current-time = “jagenOverzicht.html”> 0:00 </ b> Minuten der Jagd","je bent nu <b my-current-time=\"tellenOverzicht.html\"></b> minuten bezig met tellen":"du jagst jetzt für <b my-current-time=“app.jagenOverzicht”></b> minute","kaart":"karte","kies een taak":"wähle eine aufgabe","klik op een van onderstaande knoppen als je een hommel ziet":"klicken sie auf eine der schaltflächen unten, wenn sie eine hummel sehen","lijst":"liste","maak verbinding":"verbinden","met pollen":"mit pollen","om":"om","opnieuw":"wieder","opslaan":"speichern","stoppen":"stoppen","terug":"zurück","volgende week beter":"besser nächste Woche","zonder pollen":"ohne pollen"});
  
  gettextCatalog.baseLanguage 	= "nl_NL";
  //gettextCatalog.debug 			= true;
  //console.log(gettextCatalog);
  $ionicLoading.show({
	  template: 'Laadt...',
      duration: 1300
  })
  //security inloggen
  
  setTimeout(function(){  
	  if($state.current.name!=="app.inloggen" && !Storage.getLocalItem('userData') ){
		 //console.log(Storage.getLocalItem('userData'));
		 $state.go('app.inloggen');
	  }else{
	  	$rootScope.userData = Storage.getLocalItem('userData');
	  	//console.log($rootScope.userData);
 	  }	  
  }, 50)


 //if(typeof Storage.getLocalItem('userData')==="undefined"){
//		$state.go('inloggen');
 //}else{
	//$rootScope.userData = Storage.getLocalItem('userData');
	//console.log($scope.userData);
 //}

/**/		
  if(typeof window.localStorage["taal"]==="undefined"){
  	gettextCatalog.setCurrentLanguage('nl_NL');
	//window.localStorage["taal"] = 'nl_NL';
	localStorage.setItem('taal','nl_NL');
  }else{
	gettextCatalog.setCurrentLanguage(localStorage.getItem('taal'));
  } 
  $rootScope.taal= localStorage.getItem('taal');
	        
  if(!localStorage.getItem('size')){
 	Storage.calculateLocalStorage();
  }else{
	//Storage.remainingLocalStorage();
  }
  
   $rootScope.online = navigator.onLine;
   
      $window.addEventListener("offline", function() {
        $rootScope.$apply(function() {
          $rootScope.type 	= false;
          $rootScope.online = false;
        });
      }, false);

      $window.addEventListener("online", function() {
        $rootScope.$apply(function() {
          $rootScope.type 	= false;
          $rootScope.online = true;
        });
      }, false);
  	
  
 
  $ionicPlatform.ready(function() {
    $ionicLoading.hide().then(function(){
		
		if ('serviceWorker' in navigator) {
		  navigator.serviceWorker.register('service-worker.js').then(function(registration) {
		    //Registration was successful
		    console.log('ServiceWorker registration successful with scope: ', registration.scope);
		  }).catch(function(err) {
		    //registration failed :(
		    console.log('ServiceWorker registration failed: ', err);
		  });
		}

		
	});
   
  });
  

});