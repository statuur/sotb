
angular.module('app.services', [])
.factory('Position', ['$q', '$window', function ($q, $window) {

    'use strict';

    function getCurrentPosition() {
        var deferred = $q.defer();

        if (!$window.navigator.geolocation) {
            deferred.reject('Geolocation not supported.');
        } else {
            $window.navigator.geolocation.getCurrentPosition(
                function (position) {
                    deferred.resolve(position);
                },
                function (err) {
                    deferred.reject(err);
                });
        }

        return deferred.promise;
    }

    return {
        getCurrentPosition: getCurrentPosition
    };
}])
	
.factory('Storage', ['$window', '$log', '$http', '$ionicModal', '$ionicPopup', '$ionicActionSheet', '$httpParamSerializer', '$q', '$state', '$ionicHistory', 'Upload', 'gettextCatalog',
function($window, $log, $http, $ionicModal, $ionicPopup, $ionicActionSheet, $httpParamSerializer, $q, $state, $ionicHistory, Upload, gettextCatalog){
return {
    saveJacht : function($scope){
	    	$scope.modal.hide();
    		$ionicModal.fromTemplateUrl('templates/jagenUpload.html', {
		  				scope: $scope,
		  				animation: 'slide-in-up'
	  				}).then(function(modal) {
	  					$scope.modal = modal;
	  					$scope.modal.show();
	  					
	  					var promises = [];
	  					$scope.uploads = 0;
		  				$scope.uploading = true;
		  				
		  				angular.forEach($scope.jacht.hommels, function(data, index) {
		  				
		  				if(typeof data.foto==="string"){
	  						var promise = Upload.upload({//'userData': $scope.userData, 
								            url: 'https://dev.silenceofthebees.eu/api/hommelapp/upload/',
								            data: {'filename' : data.foto, 'position' : data.position, 'time':data.time, 'width':data.width, 'height':data.height, 'file': data.data}
								        }).then(function (resp) {
								            //console.log('uploaded. Response: ' + resp.data);
								            $scope.uploads++;
								        }, function (resp) {
								            //console.log('Error status: ' + resp.status);
								        }, function (evt) {
								            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
								            //console.log(angular.element(document.getElementById('heading')).css({"color":"red"}));
								            angular.element(document.getElementById('progressbar-'+index)).css({"width": progressPercentage+"%"});
								            angular.element(document.getElementById('progressnum-'+index)).text(progressPercentage);
								            //console.log('progress: '+foto+"- " + progressPercentage + '% ');
								        });
							promises.push(promise);
						}
							  });
	  					
	  					var jachtsessie 		= $scope.jacht;
		  				jachtsessie.userData 	= $scope.userData;
						
						//strip out the imagedata		  				
		  				angular.forEach(jachtsessie.hommels, function(data, foto) {
		  					//if(typeof jachtsessie.hommels[foto].data!=="undefined"){
		  						delete jachtsessie.hommels[foto].data;
		  					//}
		  				});
		  				
		  				//var parameters = $httpParamSerializer($scope.jachtsessie);
		  				//console.log($scope.jachtsessie);
		  				var sessie = $http({
						    method: 'POST',
						    url :'https://dev.silenceofthebees.eu/api/hommelapp/saveJachtSessie/',
						    data: $httpParamSerializer(jachtsessie),
						    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						}).then(function successCallback(response) {
							//error report from API?	
							if(response.data=="uploaded"){
								//alert('oke delete colors etc');
							}else{
								alert('error');
								console.error(response);
							}			
						}, function errorCallback(response) {
					    	alert('error');
					    	console.error(repsonse);
					    	//if(response.status==-1){
				    			//error function
				    		//}
				    		
					  	});
		  				
		  				promises.push(sessie);
	  					
	  					$q.all(promises).then(function(){
		  					$scope.uploadready = true;
		  					console.log("Uploads ready");
		  					$scope.uploading = false;
		  					setTimeout(function(){
			  					localStorage.removeItem('jacht');
			  					$scope.modal.hide();
			  					$scope.uploading = false;
			  					$state.go('app.kiesEenTaak');
			  					//$scope.jacht = undefined;
			  					
		  					}, 2500)
		  					
	  					});	  					
	  				});    
    },
    
    /** 
	* Save an image online or offline based on connection type 
	**/
    /*saveImage: function($scope, src){
	    
	    if($scope.online){ //is connected
		 Upload.upload({
            url: 'https://dev.silenceofthebees.eu/api/hommelapp/upload/',
            data: {'filename' : $scope.filename, 'userData': $scope.userData, file: src}
        }).then(function (resp) {
            console.log('uploaded. Response: ' + resp.data);
            $scope.sluitPopover();
            //+1
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ');
        });
		
		}else{
			console.log("save to localstorage");
			localStorage.setItem($scope.filename, src);
		}
		
    },
    */
    /** 
	* Calculates the size of window.localstorage 
	**/
    
    calculateLocalStorage: function (x) {
      
	if (!localStorage) {
	    console.log('unavailable');
	} else {
	    var max = 'x', s;
	    var lower_bound = 1, upper_bound = 1, middle;
	
	    // determine lower and upper bound
	    try {
	        while (true) {
	            localStorage.setItem('test', max);
	            lower_bound = upper_bound;
	            upper_bound *= 2;
	            max = max + max;
	        }
	    } catch (e) {
	    }
	
	    // do a binary search
	    while (upper_bound - lower_bound > 2) {
	        try {
	            middle = (lower_bound + upper_bound) / 2;
	            s = max.substr(0, middle);
	            localStorage.setItem('test', s);
	            lower_bound = middle;
	        } catch (e) {
	            upper_bound = middle;
	        }
	    }
	
	    //console.log('size=' + lower_bound + '/' + upper_bound + '=' + (lower_bound / 1024).toFixed(1) + 'k');
	    localStorage.removeItem('test');
	    localStorage.removeItem('checked');
	    localStorage.setItem('size', (lower_bound / 1024).toFixed(1));
	    //return (lower_bound / 1024).toFixed(1) + 'k';
	    return (lower_bound / 1024).toFixed(1);
	}
	},
    
    /** Calculates the remaining size of window.localstorage **/
    remainingLocalStorage : function(){
	    var data = '';
	
	    console.log('Current local storage: ');
	
	    for(var key in window.localStorage){
	
	        if(window.localStorage.hasOwnProperty(key)){
	            data += window.localStorage[key];
	            console.log( key + " = " + ((window.localStorage[key].length * 16)/(8 * 1024)).toFixed(2) + ' KB' );
	        }
	
	    }
	
	    console.log(data ? '\n' + 'Total space used: ' + ((data.length * 16)/(8 * 1024)).toFixed(2) + ' KB' : 'Empty (0 KB)');
	    console.log(data ? 'Approx. space remaining: ' + (localStorage.getItem('size') - ((data.length * 16)/(8 * 1024)).toFixed(2)) + ' KB' : '5 MB');
		return (localStorage.getItem('size') - ((data.length * 16)/(8 * 1024)).toFixed(2));
	},
    
    /** get all items in locaStorage and return as object **/
    getLocalItems: function () {
		
		//console.log(Object.keys(window.localStorage));
		return Object.keys(window.localStorage);
	}, 
	
	/** get an item from locaStorage and return it **/	
    getLocalItem: function (item) {
		
		if(typeof localStorage.getItem(item)==="undefined"){
			return false;
		}
		//console.info(JSON.parse(localStorage.getItem(item)));
		
		//check if item is actually an json object or not
		return JSON.parse(localStorage.getItem(item));
	}, 
  	 
  	/** save an item in locaStorage and return true **/  
    saveLocalItem: function (key, value) {
      //console.log(typeof value);
      //localStorage.removeItem('formData');
      window.localStorage[key] = (typeof value==="object") ? JSON.stringify(value) : value; 
      //console.info(window.localStorage[key]);
      return true;
    },
    
     saveAnalyse: function($scope){
		this.checkConnectivity($scope, 'saveHommelTellen', 'app.kiesEenTaak').then(function() {
			console.info('Connected to internet, saving....');
			var merged = {"Gemaaid gras"	:	"gemaaidgras", 
						"Ongemaaid gras"	:	"ongemaaidgras",  
						"Bomen"				:	"bomen",  
						"Bloeiende planten"	:	"bloeiendeplanten",  
						"Bebouwing"			:	"bebouwing",  
						"Struiken"			:	"struiken"}

			//filter out unused colors and assign value
			angular.forEach($scope.colors, function(data, index) {
				//console.log(merged[$scope.colors[index].type]);
				$scope.colors[index].typevalue = merged[$scope.colors[index].type];
				if(!data.enabled){
					delete $scope.colors[index];
				}
			})	
			
			
			
			 $scope.uploadPopup = $ionicPopup.show({
			    template: '<div style="text-align:center; width:100%;background:#f2f2f2"><div id="progressbar" style="height:2px;width:0%;background:#000;"></div><div style="font-size:10px"><span id="progressnum">0</span>%</div></div>',
			    title: 'Analyse wordt opgeslagen',
			    scope: $scope
			    });
		
		
			Upload.upload({//'userData': $scope.userData, 
				url: 'https://dev.silenceofthebees.eu/api/hommelapp/saveAnalyse/',
				data: {'filename' : $scope.image.filename, 'time':$scope.image.time, 'colors': $scope.colors, 'width':$scope.image.naturalwidth, 'height':$scope.image.naturalHeight, 'file': $scope.image.data, 'userData':$scope.userData}
			}).then(function (resp) {
				console.log('uploaded. Response: ' + resp.data);
				$scope.uploadPopup.close();
				
				
				$scope.uploadPopup = $ionicPopup.show({
			    template: '<div style="text-align:center; width:100%;background:#f2f2f2">Je gaat nu terug naar het beginscherm</div>',
			    title: 'Analyse opgeslagen',
			    scope: $scope
			    });
				
				setTimeout(function(){$scope.uploadPopup.close();
					$scope.colors = undefined;
					$scope.image  = undefined;
					$state.go("app.kiesEenTaak", null, {
						location: 'replace'
					})	
				}, 3000)
				
				
			}, function (resp) {
				console.log('Error status: ' + resp.status);//<--- Error messagehere
			}, function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				angular.element(document.getElementById('progressbar')).css({"width": progressPercentage+"%"});
				angular.element(document.getElementById('progressnum')).text(progressPercentage);
			});
		/**/	
	
		
		//if the user is not online and make decisions about what to do next
		}, function(reason) {
			if(reason==="kieseentaak"){
				$scope.hideSheet2();
				//$scope.jacht = undefined;//<----!
				if($scope.modal && $scope.modal.isShown()){
					$scope.modal.hide();
				}
							
				$ionicHistory.nextViewOptions({
					disableBack: true
				});
							
				$state.go("app.kiesEenTaak", null, {
					location: 'replace'
				})			
			}else{
				console.info(reason);
			}
		}, function(update) {
			console.info('Notificatie: ' + update);
		});
	},
    
	checkConnectivity: function($scope){
		var deferred = $q.defer();
		var confirmPopup;
		if(!$scope.online){
				
			$scope.hideSheet2 = $ionicActionSheet.show({
		     buttons: [
		       { text: gettextCatalog.getString('maak verbinding')}
		     ],
		    // destructiveText: 'Delete',
		     destructiveText: gettextCatalog.getString('het lukt niet om nu verbinding te maken'),
		     titleText: gettextCatalog.getString('Om te kunnen opslaan moet je een werkende internetverbinding hebben. Loop naar een wifipunt of maak op een andere manier verbindng.'),
		     cancelText: gettextCatalog.getString('annuleren'),
		     cancel: function(event) {
		          // add cancel code..
		           setTimeout(function(){
			           if(!confirmPopup){
				          	deferred.reject('User cancels Actionsheet'); 
			           };
		           }, 500);
		           //console.log(event);
		           //
		        },
		     buttonClicked: function(index) {
		        	$scope.hideSheet2(); 
		        	deferred.reject('User tries to connect first');
		     },
		     
		     destructiveButtonClicked: function(){
			    deferred.notify('Popup');	
			    confirmPopup = $ionicPopup.confirm({
				  title: gettextCatalog.getString('Je gegevens gaan op deze manier helaas verloren.'), // String. The title of the popup.
				  //cssClass: '', // String, The custom CSS class name
				  subTitle: gettextCatalog.getString('Weet je zeker dat je af wilt sluiten?'), // String (optional). The sub-title of the popup.
				  //template: '', // String (optional). The html template to place in the popup body.
				  //templateUrl: '', // String (optional). The URL of an html template to place in the popup   body.
				  cancelText: gettextCatalog.getString('Nee'), // String (default: 'Cancel'). The text of the Cancel button.
				  cancelType: 'button-default', // String (default: 'button-default'). The type of the Cancel button.
				  okText: gettextCatalog.getString('Ja'), // String (default: 'OK'). The text of the OK button.
				  okType: 'button-energized', // String (default: 'button-positive'). The type of the OK button.
				}).then(function(res) {
					if(res) {
						//this is the part where data isnt beeing saved and the user quits tasks
						deferred.reject('kieseentaak');//not saving
						
					}else {
					      deferred.reject('User tries to connect first after reading confirm popup');
					      confirmPopup = undefined;
					}
			});
				
				$scope.hideSheet2(); 
					 
		     },
		     cssClass : 'center'
    		});
			
			
			
		}else{
			//deferred.notify('About to greet.');
			deferred.resolve('verbonden');
		}
		
		
		
		return deferred.promise;
	},
    
   saveHommelTellen: function(scope){
	  	angular.extend(scope.hommelkast, scope.userData)
	  	
	  	//scope.hommelkast.position = Object.keys(scope.hommelkast.position).map(function(key) {
	  	//	return scope.hommelkast.position[key];
  		//});
	  	//console.log($httpParamSerializer(scope.hommelkast));
	  	
	  	$http({
			method: 'POST',
			url :'https://dev.silenceofthebees.eu/api/hommelapp/saveHommeltellen/',
			data: $httpParamSerializer(scope.hommelkast),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function successCallback(response) {
				//console.log(response);
				
				if(response.data){
					localStorage.removeItem('hommelkast');
					scope.modal.hide();
					scope.hommelkast = undefined;
					$state.go('app.kiesEenTaak');
				}
							
			}, function errorCallback(response) {
				if(response.status==-1){
				    //error function
				}
		});  			
    }
    
   
  };

	//return true;
}])
