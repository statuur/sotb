"use strict";
var cModal ="";
angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    .state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/menu.html',
		controller: 'AppCtrl'
	})

    .state('app.inloggen', {
      url: '/inloggen/',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/inloggen.html',
	  	controller: "inloggenCtrl"
	  }}
    })
    
    .state('app.welkom', {
      url: '/welkom',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/welkom.html',
	  	controller: "welkomCtrl"
	  }}
    }) 
     
    .state('app.kiesEenTaak', {
      url: '/kies-een-taak',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/kiesEenTaak.html',
	  	controller: "kiesEenTaakCtrl"
	  }}
    })
    
    .state('app.analyseIntro', {
      url: '/analyse-intro',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/analyseIntro.html',
	  	controller: "analyseintroCtrl"
	  }}
    })    
    .state('app.analyseStap1', {
      url: '/analyse-stap1',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/analyseStap1.html',
	  	controller: "analyseStap1Ctrl"
	  }}
    })
    /*
    .state('app.analyseStap2', {
      url: '/analyse-stap2',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/analyseStap2.html',
	  	controller: "analyseStap2Ctrl"
	  }}
    })
    */
    .state('app.hommelkast', {
      url: '/hommelkast',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/hommelkast.html',
	  	//controller: "hommmelkastCtrl"
	  }}
    })
    
    .state('app.tellenWeer', {
      url: '/tellen-weer',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/tellenWeer.html',
	  	controller: "tellenWeerCtrl"
	  }}
    })
    
    .state('app.tellenWind', {
      url: '/tellen-wind',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/tellenWind.html',
	  	controller: "tellenWindCtrl"
	  }}
    
    })
    
    .state('app.telAlleHommels', {
      url: '/tel-alle-hommels',
       views: {
	  	'menuContent': {
	  	templateUrl: 'templates/telAlleHommels.html'
	  	
	  }}
    })
    
    .state('app.tellenOverzicht', {
      url: '/tellen-overzicht',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/tellenOverzicht.html',
	  	controller: "tellenOverzichtCtrl"
		}}
    })
    
    .state('app.hommeljacht', {
      url: '/hommeljacht',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/hommeljacht.html',
	  	controller: "hommeljachtCtrl"
	}}
      
    })

     .state('app.jagenWeer', {
      url: '/jagen-weer',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/jagenWeer.html',
	  	controller: "jagenWeerCtrl"
	  }}
    })
    
     .state('app.jagenWind', {
      url: '/jagen-wind',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/jagenWind.html',
	  	controller: "jagenWindCtrl"
	  }}
    })
    
    .state('app.tellen', {
      url: '/tellen',
      //templateUrl: 'templates/tellen.html',
      //controller: 'tellenCtrl'
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/tellen.html',
	  	controller: 'tellenCtrl'
	  }}
    })
    
    .state('app.jagen', {
      url: '/jagen',
      views: {
	  	'menuContent': {
	  	templateUrl: 'templates/jagen.html',
	  	controller: "jagenCtrl as jg"
	}}

    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/kies-een-taak');


});