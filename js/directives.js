angular.module('app.directives', [])
.directive('myCurrentTime', ['$interval', '$state', '$ionicModal', '$filter', 'Storage',
      function($interval, $state, $ionicModal, $filter, Storage) {
        // return the directive link function. (compile function not needed)
        return function(scope, element, attrs) {
          //console.log(scope.$parent);
          var stopTime,
              minutes=0, 
              seconds=0,
              counter=0,
              tijd,
              maxtime = 12,
              seconds_c; // so that we can cancel the time updates
			
          // used to update the UI
          function updateTime() {
            counter++;
            minutes 	= Math.floor(counter / 60);
            seconds 	= counter - (minutes * 60);
            seconds_c	= (seconds < 10) ? "0"+seconds : seconds;
            scope.tijd	= minutes+":"+seconds_c;
            element.text(scope.tijd);
            
            if(counter===maxtime){
	            //reset values save state
	            $interval.cancel(stopTime);
	            
	            	
		            var parent = (typeof scope.$parent.jacht==="undefined")? scope.$parent.hommelkast : scope.$parent.jacht;
		            
		            scope.eindtijd				= new Date().getTime();
		            scope.seconden 				= parseInt((scope.eindtijd - parent.starttijd) / 1000);
					scope.minuten				= Math.floor(scope.seconden / 60);
					scope.seconds 				= scope.seconden - (scope.minuten * 60);
					scope.seconds_c				= (scope.seconds < 10) ? "0"+scope.seconds : scope.seconds;
					scope.$parent.totaaltijd	= scope.minuten+":"+scope.seconds_c;
		            
					parent.eindtijd				= scope.eindtijd;	
	            
	            //Storage.saveLocalItem('jacht', scope.$parent.jacht);
	            
	            $ionicModal.fromTemplateUrl('templates/'+attrs.myCurrentTime, {
		  				scope: scope.$parent,
		  				animation: 'slide-in-up'
	  				}).then(function(modal) {
	  					
	  					if(typeof scope.$parent.prepare==="function"){
	  						scope.$parent.prepare();
	  					}
	  					scope.$parent.modal = modal;
	  					scope.$parent.modal.show();	
	  					});
  						
            }
    
          }

          // watch the expression, and update the UI on change.
          scope.$watch(attrs.myCurrentTime, function(value) {
            updateTime();
          });

          stopTime = $interval(updateTime, 1000);

          // listen on DOM destroy (removal) event, and cancel the next UI update
          // to prevent updating time after the DOM element was removed.
          element.on('$destroy', function() {
            $interval.cancel(stopTime);
          });
        }
      }])

    
	
.directive('foto', ['$rootScope', '$filter', 'Position', function($rootScope, $filter, Position) {
  return {
    restrict: 'A',
    //replace:false,
    //template: '',
    //scope: scope,
    link: function(scope, element, attrs) {
	
	Position.getCurrentPosition().then(function(position) {scope.$parent.position = position});
	scope.imageContainer 	= angular.element(document.getElementById('image-container'));
    scope.ioncontent		= angular.element(document.getElementsByClassName('ionic-scroll'));
	scope.input 			= angular.element(document.getElementById('capture'));
	scope.reader 			= new FileReader();
	
	element[0].onclick 		= function(){
			scope.input[0].click();	
		}
	
	function jagen(){
		//get current position
		Position.getCurrentPosition().then(function(position) {scope.$parent.position = position});
		
		scope.filename = scope.$parent.userData.school.post_name+"/"+attrs.controller+"/"+scope.$parent.userData.user_nicename+"-"+$filter('date')(new Date(), 'yyyyMMdd-HHmmss')+".png";
		var objURL 			= URL.createObjectURL(event.target.files[0]);
		
		scope.reader.onload 		= function (e) {
		   			var image  				= new Image();
		   			image.src 				= objURL;
				
					image.addEventListener('load', function(event) {
		   			var canvas 		= document.createElement('canvas');
						canvas.width 	= image.width;
						canvas.height 	= image.height;
						var ctx 		= canvas.getContext('2d');

						// crop and scale image for final size
						ratio 		= Math.min(image.width / canvas.width, image.height / canvas.height),
						sw 			= canvas.width * ratio,
						sh 			= canvas.height * ratio;
						sx 			= (image.width - sw) / 2,
						sy 			= (image.height - sh) / 2;
						ctx.drawImage(image, sx, sy, sw, sh, 0, 0, canvas.width, canvas.height);

						/**** miniatuur ****/
		   				var miniatuur 	= document.createElement('canvas');
						var mtx 		= miniatuur.getContext('2d'),
						orientation		= (image.width > image.height) ? "l" : "p";
						ratio 			= (orientation=="l") ? image.height / image.width :  image.width / image.height,
						miniatuur.width = (orientation=="l") ? 80 : 80 * ratio,
						miniatuur.height= (orientation=="p") ? 80 : 80 * ratio;
						mtx.drawImage(image, 0,0,miniatuur.width, miniatuur.height);

						var origineelURL = canvas.toDataURL();
						var miniatuurURL = miniatuur.toDataURL();
						scope.$parent.$apply(function () {
						var position = (typeof scope.$parent.position!=="undefined") ? {"latitude":scope.$parent.position.coords.latitude, "longitude":scope.$parent.position.coords.longitude} : {}
		   				scope.$parent.jacht.hommels[scope.$parent.jacht.shommels] = {"time":new Date().getTime(), "position": position, "foto": scope.filename, "studentID": scope.$parent.userData.ID, "schoolID": scope.$parent.userData.school.ID, "width":image.width,"height":image.height, "data": origineelURL, "url":image.src, "fade":false, "miniatuur": {"width":miniatuur.width,"height":miniatuur.height, "url":miniatuurURL}}
		   				});
		   				scope.$parent.jacht.fhommels++;
		   				scope.$parent.jacht.shommels++;
		   				scope.$parent.disabled		= false;		   				
		   				});	
						
						scope.imageContainer[0].appendChild(image);
		   		}
		   		
		   		scope.reader.readAsDataURL(event.target.files[0]);
		   		window.URL.revokeObjectURL(image.src);		
	}
	
	function analyse(event){
		var canvas 		= document.createElement('canvas'),
		ctx 			= canvas.getContext('2d'),
		image  			= new Image();
		image.id		= "header-image";
		image.src 		= URL.createObjectURL(event.target.files[0]);
		image.filename 	= scope.$parent.userData.school.post_name+"/"+attrs.controller+"/"+scope.$parent.userData.user_nicename+"-"+$filter('date')(new Date(), 'yyyyMMdd-HHmmss')+".png";
		image.time 		= new Date().getTime()
		
		
		image.onload = function(event){
			var swatches 	= new Vibrant(image).swatches();
			var ratio 		= image.naturalHeight / image.naturalWidth;
			
			canvas.width 	= 1024;//image.naturalWidth,
			canvas.height 	= 768;//image.width * ratio;//image.naturalHeight;
			
			//sw 				= canvas.width * ratio,
			//sh 				= canvas.height * ratio,
			//sx 				= (image.width - sw) / 2,
			//sy 				= (image.height - sh) / 2;
			ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
			image.data 			= canvas.toDataURL();			
			
			//scope.imageContainer[0].style.height = (image.width * ratio)+"px";
			//style = window.getComputedStyle(scope.ioncontent[0]),
			//scope.ioncontent[0].style.top	= 44+(image.width * ratio)+"px";
			//scope.ioncontent[0].style.height= style.height - parseInt(scope.ioncontent[0].style.top)+"px";
			angular.forEach(swatches, function(value, key) {
				if(swatches[key]){
					scope.$parent.tpopulation = scope.$parent.tpopulation + value.population;
					scope.$parent.colors.push({"enabled": true, "name": key, "color":swatches[key].getHex(), "population":value.population, "type": ""});
				}
			});

			angular.forEach(scope.$parent.colors, function(value, index) {
					scope.$parent.colors[index].percentage 	= (scope.$parent.colors[index].population  / scope.$parent.tpopulation * 100).toFixed(0);
					scope.$parent.colors[index].opercentage	= (scope.$parent.colors[index].population  / scope.$parent.tpopulation * 100).toFixed(0);		
			});
			
			//console.log(scope.$parent.colors);
			
			scope.$parent.$apply(function () {
				scope.$parent.image = image;
				scope.$parent.colors = scope.$parent.colors
			});	
		};
		
		scope.imageContainer[0].appendChild(image);
		
		scope.$parent.stap++;
	}	
	
	scope.input[0].onchange = function(event){
		if (event.target.files.length > 0 && event.target.files[0].type.indexOf('image/') == 0) {
			//disable the button
			scope.$parent.disabled		= true;
			
			//check for the proper file extensions
			if ( !(/\.(png|jpeg|jpg|gif)$/i).test(event.target.files[0].name)) {
		   		alert(event.target.files[0].name+" is niet een ondersteund bestandstype");
		   			return false;
		   	}
		   	//direct to the proper function via the attrs.controller value
		   	switch(attrs.controller){
			   	case "jacht":
			   	jagen();
			   	break;
			   	
			   	case "analyse":
			    analyse(event);
			   	break;
			   	
		   	}
		}	
	}		
}
}}])

/*
  This directive is used to disable the "drag to open" functionality of the Side-Menu
  when you are dragging a Slider component.
*/
.directive('disableSideMenuDrag', ['$ionicSideMenuDelegate', '$rootScope', function($ionicSideMenuDelegate, $rootScope) {
    return {
        restrict: "A",  
        controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {

            function stopDrag(){
              $ionicSideMenuDelegate.canDragContent(false);
            }

            function allowDrag(){
              $ionicSideMenuDelegate.canDragContent(true);
            }

            $rootScope.$on('$ionicSlides.slideChangeEnd', allowDrag);
            $element.on('touchstart', stopDrag);
            $element.on('touchend', allowDrag);
            $element.on('mousedown', stopDrag);
            $element.on('mouseup', allowDrag);

        }]
    };
}])

/*
  This directive is used to open regular and dynamic href links inside of inappbrowser.
*/
.directive('hrefInappbrowser', function() {
  return {
    restrict: 'A',
    replace: false,
    change: '&',
    transclude: false,
    link: function(scope, element, attrs) {
      var href = attrs['hrefInappbrowser'];

      attrs.$observe('hrefInappbrowser', function(val){
        href = val;
      });
      
      element.bind('click', function (event) {

        window.open(href, '_system', 'location=yes');

        event.preventDefault();
        event.stopPropagation();

      });
    }
  };
      
  
})

