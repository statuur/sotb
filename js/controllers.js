"use strict";


angular.module('app.controllers', [])
 
.controller('AppCtrl', ['$scope', '$ionicSideMenuDelegate', 'gettextCatalog', '$state',
function ($scope, $ionicSideMenuDelegate, gettextCatalog, $state) {
 	
 	//console.log(gettextCatalog);
 	$scope.showRightMenu = function(){
	 	 $ionicSideMenuDelegate.toggleRight();
 	}
 	
 	$scope.uitloggen = function(){
	 	localStorage.removeItem('userData');
	 	$ionicSideMenuDelegate.toggleRight();
	 	$state.go("app.inloggen");
	 	
 	}
 	$scope.ChangeLanguage = function(lang){
		$scope.taal= lang;
		gettextCatalog.setCurrentLanguage(lang);
		window.localStorage["taal"] = lang;
	}
 	
}]) 
 
.controller('inloggenCtrl', ['$scope', '$stateParams', '$http', '$httpParamSerializerJQLike', '$ionicPopup', '$timeout', 'Storage', '$state',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $httpParamSerializerJQLike, $ionicPopup, $timeout, Storage, $state) {
	
	if(Storage.getLocalItem('userData')){
		$state.go('app.welkom');
	}
	//get schoolAlias otherwhise error out
	if($stateParams.user==="undefined"){
		//error out
	}
	$scope.iform = {};
	$scope.iform.email 		= "lotus@statuur.nl";
	$scope.iform.password 	= "aybJnxYF81T9hjb8";
	$scope.disabled 		= false;
	
	
	
	$scope.inloggen  = function(form){
		//form.iform.login = $stateParams.login
		$scope.disabled = true;
		$http({//http://dev.silenceofthebees.eu/api/hommelapp/test/
			    method: 'POST',
			    url :'https://dev.silenceofthebees.eu/api/hommelapp/login/',
			    data: $httpParamSerializerJQLike(form.iform),
			    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function successCallback(response) {
				//console.log(response.data);
				if(response.data.error){
					var ionicPopup = $ionicPopup.show({
				        title: 'Mislukt',
				        template: 'de door jouw ingevulde gegevens zijn bij ons niet bekend.',
				        scope: $scope,
				        cssClass: 'codeQR',
				        buttons: [{ text: 'opnieuw', type: 'button-energized'}]
					});
		
					$timeout(function() {
						ionicPopup.close(); //close the popup after 3 seconds for some reason
			  		}, 3000);
			  		$scope.disabled = false;
				}else{
					delete response.data.status; //delete response.data.allcaps; 
					Storage.saveLocalItem("userData", response.data);// <---------
					$state.go('app.welkom', {obj:'niks'});
				}
								
			}, function errorCallback(response) {
		    	if(response.status==-1){
	    			//error function
	    		}
	    		
		  	});
	}
	
}])
   
.controller('analyseintroCtrl', ['$scope', '$stateParams', 'Storage',   //'$colorThief',
function ($scope, $stateParams, Storage) {
			$scope.analyse = (Storage.getLocalItem('analyse')===(undefined || null)) ? {} : Storage.getLocalItem('analyse'); 
			//weg?
			/*
			$scope.icolor = [0,0,0,0];
			$scope.getColor = function(event, obj){
			var image 		= document.getElementById('kaart');
				var canvas 		= document.createElement('canvas');
				canvas.width 	= image.naturalWidth;
				canvas.height 	= image.naturalWidth;
				canvas.getContext('2d').drawImage(image, 0, 0, image.naturalWidth, image.naturalWidth);
				$scope.icolor = canvas.getContext('2d').getImageData(event.offsetX, event.offsetY, 1, 1).data;
				console.log($scope.icolor);
			}
			*/
			
	
}])
.controller('analyseStap1Ctrl', ['$scope', '$stateParams', 'Storage', '$filter',  //'$colorThief',
function ($scope, $stateParams, Storage, $filter) {
		//$scope.analyse 	= (Storage.getLocalItem('analyse')===(undefined || null)) ? {} : Storage.getLocalItem('analyse'); 
		$scope.tpopulation 		= 0;
		$scope.disabled 		= false;	
		$scope.stap 			= 1;	
		$scope.hasEmptyValues 	= true;
		$scope.colors			= [];
		$scope.legenda 			= [
		{"name":"Gemaaid gras"		, "disabled" : false}, 
		{"name":"Ongemaaid gras"	, "disabled" : false},  
		{"name":"Bomen"				, "disabled" : false},  
		{"name":"Bloeiende planten"	, "disabled" : false},  
		{"name":"Bebouwing"			, "disabled" : false},  
		{"name":"Struiken"			, "disabled" : false}
		];
		
		$scope.$watch('colors', function(newValue, oldValue) {
				if(typeof newValue==="undefined" || !newValue.length || !oldValue.length)return false
				/**
				 * 1: if a user resets a color by clicking on the X
				 * 2: calulates total percentagen should be < 100%
				 * if it is above 100
				 **/
				var percentage = 0;
				angular.forEach($scope.colors, function(data, index) {
					
					if(newValue[index].enabled){
						percentage = percentage + parseInt($scope.colors[index].percentage);
					}
					if(newValue[index].enabled!=oldValue[index].enabled && !newValue[index].enabled){
						$scope.resetLegenda(index);
					}
				});
				if(percentage > 100){
					//console.log("nu dus");
				}
				/** 
				* calculate if all values are set properly
				* all the colors should be assigned
				* percentage of the sum of used colors should not be above 100%
				**/
				angular.forEach($scope.colors, function(data, index) {
					if(data.type==="" && data.enabled){
						$scope.hasEmptyValues = true;
						return false;
					}
					$scope.hasEmptyValues = false;
				});
				
				//console.log($scope.colors);
		}, true);
		
		$scope.setLegenda = function(index){
			$scope.colors[index].type = $scope.colors[index].type.name
			var selected = $filter('filter')($scope.legenda, {'name':$scope.colors[index].type}) 
			if(selected[0]){
				selected[0].disabled=true
			}
		}

		$scope.resetLegenda = function(index){
			$scope.colors[index].type = "";
			var selected = $filter('filter')($scope.legenda, {'name':$scope.colors[index].type}) 
			if(selected[0]){
				selected[0].disabled=false
			}
		}
		
		$scope.saveData = function(index){
			Storage.saveAnalyse($scope);				
		}	
}])
  
.controller('kiesEenTaakCtrl', ['$scope', '$stateParams', '$rootScope', 'Storage',
function ($scope, $stateParams, $rootScope, Storage) {
	$rootScope.userData = Storage.getLocalItem('userData');
}])
   
.controller('hommelkastCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

}])
   
.controller('tellenWeerCtrl', ['$scope', '$stateParams', 'Storage', 
function ($scope, $stateParams, Storage) {
	
	$scope.weertypes			= {0: "harde regen", 20:"beetje regen", 40:"zwaar bewolkt", 60:"beetje bewolkt", 80:"zonnig", 100:"heel zonnig"}	
	$scope.hommelkast 		= (Storage.getLocalItem('hommelkast')===(undefined || null)) ? {"weer": 10} : Storage.getLocalItem('hommelkast'); 
	console.log($scope.hommelkast );
	//parseInt($scope.num1)
	$scope.$watch('hommelkast.weer', function(newValue, oldValue) {
		var floored = Math.floor($scope.hommelkast.weer / 20) * 20;
		$scope.weer		= $scope.weertypes[floored ];
		Storage.saveLocalItem("hommelkast",$scope.hommelkast);
	});
	

	$scope.$on('$destroy', function() {
		//console.log($scope.hommelkast);
	});
	
}])
   
.controller('tellenWindCtrl', ['$scope', '$stateParams', 'Storage', 
function ($scope, $stateParams, Storage) {
	$scope.hommelkast 	= Storage.getLocalItem('hommelkast'); 
	$scope.winden	= {0: "windstil", 20:"bijna geen wind", 40:"beetje wind", 60:"matige wind", 80:"harde wind", 100:"storm"};	
	
	$scope.$watch('hommelkast.wind', function(newValue, oldValue) {
		var floored = Math.floor($scope.hommelkast.wind / 20) * 20;
		$scope.wind		= $scope.winden[floored];
		Storage.saveLocalItem('hommelkast', $scope.hommelkast);
	});
	
}])
 
.controller('telAlleHommelsCtrl', ['$scope', '$stateParams', 
function ($scope, $stateParams) {


}]) 
 
.controller('tellenCtrl', ['$scope', '$stateParams', '$ionicActionSheet', '$ionicPopup', '$interval', '$filter', '$state', 'Storage', 
function ($scope, $stateParams, $ionicActionSheet, $ionicPopup, $interval, $filter, $state, Storage) {
	
	if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(function(position){
	      $scope.$apply(function(){
	        $scope.position = position;
	        $scope.hommelkast.position = {"latitude":position.coords.latitude, "longitude":position.coords.longitude};
	        console.log( $scope.hommelkast.position );
	      });
	    });
  	}
	
	//thommels can't be negative value
 
 
	$scope.hommelkast 				= Storage.getLocalItem('hommelkast');
	$scope.hommelkast.starttijd		= new Date().getTime();
	$scope.hommelkast.eindtijd		= 0;
	$scope.hommelkast.met_pollen 	= 0;
	$scope.hommelkast.zonder_pollen = 0;
	
	$scope.recordPollen 			= function(pollen){
		if(pollen){
			$scope.hommelkast.met_pollen = $scope.hommelkast.met_pollen + 1
		}else{
			$scope.hommelkast.zonder_pollen = $scope.hommelkast.zonder_pollen + 1
		}
		Storage.saveLocalItem("hommelkast",$scope.hommelkast);
	}	
	
	$scope.showActionSheet 			= function(){
	// Show the action sheet
		   var hideSheet = $ionicActionSheet.show({
		     buttons: [
		       { text: 'doorgaan' }
		     ],
		    // destructiveText: 'Delete',
		     destructiveText: 'stoppen',
		     titleText: 'Weet je zeker dat je wilt stoppen?',
		     cancelText: 'Annuleren',
		     cancel: function() {
		          // add cancel code..
		        },
		     buttonClicked: function(index) {
		       hideSheet();
		       console.log(index)
		       return true;
		     },
		     
		     destructiveButtonClicked: function(){
			     hideSheet();
			     $scope.hommelkast.eindtijd		= new Date().getTime();
			     Storage.saveLocalItem("hommelkast",$scope.hommelkast);
				 $state.go('app.tellenOverzicht');
		     },
		     cssClass : 'center'
    		});
	}
	
	
  	
  	$scope.opslaan = function() {
    	
    	if(!$scope.online){
	    	var hideSheet2 = $ionicActionSheet.show({
		     buttons: [
		       { text: 'maak verbinding'}
		     ],
		    // destructiveText: 'Delete',
		     destructiveText: 'het lukt niet om nu verbinding te maken',
		     titleText: 'Om te kunnen opslaan moet je een werkende internetverbinding hebben. Loop naar een wifipunt of maak op een andere manier verbindng.',
		     cancelText: 'annuleren',
		     cancel: function() {
		          // add cancel code..
		        },
		     buttonClicked: function(index) {
		       console.log(index)
		       return true;
		     },
		     
		     destructiveButtonClicked: function(){
			    var confirmPopup = $ionicPopup.confirm({
				  title: 'Je gegevens gaan op deze manier helaas verloren.', // String. The title of the popup.
				  //cssClass: '', // String, The custom CSS class name
				  subTitle: 'Weet je zeker dat je af wilt sluiten?', // String (optional). The sub-title of the popup.
				  //template: '', // String (optional). The html template to place in the popup body.
				  //templateUrl: '', // String (optional). The URL of an html template to place in the popup   body.
				  cancelText: 'Nee', // String (default: 'Cancel'). The text of the Cancel button.
				  cancelType: 'button-default', // String (default: 'button-default'). The type of the Cancel button.
				  okText: 'Ja', // String (default: 'OK'). The text of the OK button.
				  okType: 'button-energized', // String (default: 'button-positive'). The type of the OK button.
				}).then(function(res) {
					if(res) {
						hideSheet2(); 
						$scope.jacht = undefined;
						if($scope.modal.isShown()){
							$scope.modal.hide();
						}
						$ionicHistory.nextViewOptions({
							disableBack: true
						});
						$state.go('app.kiesEenTaak', null, {
							location: 'replace'
						})
					}
					// else {
					  //     console.log('Nee');
					   // }
					});
				
				hideSheet2(); 
					 
		     },
		     cssClass : 'center'
    		});
	    }else{
			//save to database
			//also save empty jachts?
			Storage.saveHommelTellen($scope);
			
	    }
	    
	    
  	};
  	
  	

}])
   //WEG?
.controller('tellenOverzichtCtrl', ['$scope', '$stateParams', '$state', '$ionicModal', '$http','Storage',
function ($scope, $stateParams, $state, $ionicModal, $http, Storage) {
	$scope.hommelkast = Storage.getLocalItem("hommelkast");
	
	
	$scope.seconden = parseInt(($scope.hommelkast.eindtijd - $scope.hommelkast.starttijd) / 1000);
	$scope.minuten	= Math.floor($scope.seconden / 60);
    $scope.seconds 	= $scope.seconden - ($scope.minuten * 60);
    $scope.seconds_c= ($scope.seconds < 10) ? "0"+$scope.seconds : $scope.seconds;
    $scope.totaaltijd= $scope.minuten+":"+$scope.seconds_c;

	//console.log($scope.tijd);
	
	$ionicModal.fromTemplateUrl('templates/tellen-aanpassen.html', {
    scope: $scope,
    animation: 'slide-in-up',
    focusFirstInput: true,
	hardwareBackButtonClose: true
	}).then(function(modal) {
	    $scope.modal = modal;
	});
	
	$scope.openModal = function() {
    	$scope.modal.show();
  	};
  	
  	$scope.closeModal = function() {
    	$scope.modal.hide();
  	};
  	
  	$scope.opslaan = function() {
    	//alert(12);
    	if($scope.modal.isShown()){
    		$scope.modal.hide();
    		//destroy
    	}
    	//Storage.saveLocalItem('hommelkast', $scope.hommelkast);	
    	
    	//save to database
    	Storage.saveRemoteItem($scope, 'hommelkast', $scope.hommelkast);
    	
    	$state.go('app.kiesEenTaak');
  	};
}])
   
.controller('signupCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('welkomCtrl', ['$rootScope', '$scope', '$stateParams', '$state', 'Storage', 'gettextCatalog',
function ($rootScope, $scope, $stateParams, $state, Storage, gettextCatalog) {
	$rootScope.userData = Storage.getLocalItem('userData');
//	console.log(gettextCatalog);
	
	
	
}])
 //Storage.getLocalItem("userData");  
.controller('hommeljachtCtrl', ['$scope', '$stateParams', '$http',  '$httpParamSerializer', 'Storage', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $httpParamSerializer, Storage) {
	
   
	/*
	$http({
						    method: 'POST',
						    url :'https://dev.silenceofthebees.eu/api/hommelapp/saveJachtSessie/',
						    data: $httpParamSerializer({"aap":"brrt","bird":"ssssss"}),
						    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						}).then(function successCallback(response) {
							//Content-type:application/json");
							console.log(response);				
						}, function errorCallback(response) {
					    	if(response.status==-1){
				    			//error function
				    		}
				    		
					  	});
	
	*/
}])



.controller('jagenWeerCtrl', ['$scope', '$stateParams', 'Storage', 
function ($scope, $stateParams, Storage) {
	$scope.jacht 			= (Storage.getLocalItem('jacht')===(undefined || null)) ? {} : Storage.getLocalItem('jacht'); 
	$scope.weertypes		= {0: "harde regen", 20:"beetje regen", 40:"zwaar bewolkt", 60:"beetje bewolkt", 80:"zonnig", 100:"heel zonnig"}	
	//$scope.hommelkast 	= (Storage.getLocalItem('hommelkast')===(undefined || null)) ? {"weer": 10} : Storage.getLocalItem('hommelkast'); 
	//console.log($scope.jacht);
	//parseInt($scope.num1)
	$scope.$watch('jacht.weer', function(newValue, oldValue) {
		var floored = Math.floor($scope.jacht.weer / 20) * 20;
		$scope.weer		= $scope.weertypes[floored];
		Storage.saveLocalItem("jacht",$scope.jacht);
	});
	

	$scope.$on('$destroy', function() {
		//console.log($scope.hommelkast);
	});
	
}])
   
.controller('jagenWindCtrl', ['$scope', '$stateParams', 'Storage', 
function ($scope, $stateParams, Storage) {
	
	
	$scope.jacht 	= Storage.getLocalItem('jacht'); 
	$scope.winden	= {0: "windstil", 20:"bijna geen wind", 40:"beetje wind", 60:"matige wind", 80:"harde wind", 100:"storm"};	
	
	$scope.$watch('jacht.wind', function(newValue, oldValue) {
		var floored = Math.floor($scope.jacht.wind / 20) * 20;
		$scope.wind		= $scope.winden[floored];
		Storage.saveLocalItem('jacht', $scope.jacht);
	});
	
}])
 
   
.controller('jagenCtrl', ['$scope', '$stateParams', '$ionicModal', '$ionicActionSheet', '$ionicPopup', '$state', '$ionicHistory', 'Storage', 'NgMap',
function ($scope, $stateParams, $ionicModal, $ionicActionSheet, $ionicPopup, $state, $ionicHistory, Storage, NgMap) {
	
	$scope.soorten	= ["Tuinhommel","Boomhommel","Steenhommel","Akkerhommel","Weidehommel","Aardhommel","Bol-ooghommel","Wilgenhommel","Waddenhommel","Gele hommel","Heidehommel","Veenhommel","Kleine aardhommel","Grote aardhommel","Moshommel","Limburgse hommel","Grashommel","Late hommel","Boshommel","Zandhommel"];
	$scope.uploadready 		= false;
	
	$scope.tabkaart			= true;
	$scope.tablijst			= false;
	$scope.jacht 			= (Storage.getLocalItem('jacht')===(undefined || null)) ? {} : Storage.getLocalItem('jacht'); 
  	/**
	*called when the hunt is over @ jagenOverzicht.html
  	*/ 
  	$scope.prepare = function(){
	  	$scope.jacht.hommels = Object.keys($scope.jacht.hommels).map(function(key) {
	  		return $scope.jacht.hommels[key];
  		});
  	
  		$scope.polygone = [];
	  	angular.forEach($scope.jacht.hommels, function(hommel, n) {
	    	$scope.jacht.hommels[n].position.latitude	= parseFloat(hommel.position.latitude) - n/5000;
	    	$scope.jacht.hommels[n].position.longitude	= parseFloat(hommel.position.longitude) + n/5000;
	    	$scope.polygone.push([$scope.jacht.hommels[n].position.latitude, $scope.jacht.hommels[n].position.longitude]);
	    	//console.log($scope.jacht.hommels[n].position.longitude) - n/1000;
	    	//var latlng = new google.maps.LatLng(hommel.position.latitude, hommel.position.longitude);
			$scope.jacht.hommels[n].icon = (typeof hommel.miniatuur==="object") ? hommel.miniatuur.url : "http://app.silenceofthebees.eu/img/zonderpollen1.svg";
		});
		
		NgMap.getMap().then(function(map) {
	    	var bounds = new google.maps.LatLngBounds();
	    	angular.forEach($scope.jacht.hommels, function(hommel, n) {
		    	var latlng = new google.maps.LatLng(hommel.position.latitude, hommel.position.longitude);
				//console.log(hommel.position.latitude, hommel.position.longitude);
				bounds.extend(latlng);
		    });	
		    map.setCenter(bounds.getCenter());
	    	map.fitBounds(bounds);
  		});
  	}
  		
	if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(function(position){
	      $scope.$apply(function(){
	        $scope.position 		= position;
	        $scope.jacht.position 	= {"latitude":$scope.position.coords.latitude, "longitude":$scope.position.coords.longitude};//position;
	      });
	    });
  	}
  	
  	$scope.totaaltijd		= undefined;
	$scope.jacht.starttijd	= new Date().getTime();
	$scope.jacht.eindtijd	= 0;
	$scope.jacht.tellen		= 0;
	
	$scope.disabled			= false;
	
	$scope.jacht.thommels	= 0;
	$scope.jacht.fhommels	= 0;
	$scope.jacht.shommels	= 0;
	$scope.jacht.hommels	= {};
	/**/
	  	
  	
  	//thommels can't be negative value
  	$scope.$watch('jacht.thommels', function() {
       $scope.jacht.thommels = ($scope.jacht.thommels < 0) ? 0 : $scope.jacht.thommels; 
       
    });
  	
  	$scope.countHommel 		= function(){
  		
  		if (navigator.geolocation) {
	    	
	    	navigator.geolocation.getCurrentPosition(
	    	//if there is a position
	    	function success(position){
				$scope.$apply(function(){
					$scope.position = position;
					$scope.jacht.hommels[$scope.jacht.shommels] = {"time":new Date().getTime(), "position": {"latitude":$scope.position.coords.latitude, "longitude":$scope.position.coords.longitude} }
					$scope.jacht.shommels++;
					$scope.jacht.thommels++;
					$scope.disabled		= false;
	      		});
	    	}, 
	    	//if there is an error
	    	function error(err) {
				console.warn(`ERROR(${err.code}): ${err.message}`);
				$scope.jacht.hommels[$scope.jacht.shommels] = {"time":new Date().getTime(), "position": {
					"latitude"	: (typeof $scope.position.coords.latitude!=="undefined") ? $scope.position.coords.latitude : 0, 
					"longitude"	: (typeof $scope.position.coords.longitude!=="undefined") ? $scope.position.coords.longitude : 0
					} }
				$scope.jacht.shommels++;
				$scope.jacht.thommels++;
			}	
	    	);
	    	
	    	
	    	
	    	
	    //if there is no navigator.geolocation present
  		}else{
	  		$scope.jacht.hommels[$scope.jacht.shommels] = {"time":new Date().getTime(), "position": {"latitude":0, "longitude":0} }
	  		$scope.jacht.shommels++;
			$scope.jacht.thommels++;
				$scope.disabled		= false;
  		}
  		
  		
  	}
  	
  	$scope.back = function(){
	  	if($scope.modal.isShown()){
	  		$scope.modal.hide();
    		$scope.disabled	= false;
	  	}
	  	$ionicHistory.nextViewOptions({
	  		disableBack: true
		});
	  	$state.go('app.kiesEenTaak', null, {
	  		location: 'replace'
		})
  	}
  
  	
  	
  	
  	$scope.wis = function(index){
	  	//console.log($scope.jacht.fhommels);
	  	//console.log($scope.jacht.fhommels["foto-"+index]);
	  	
	  	$scope.jacht.fhommels["foto-"+index].fade = true;
	  	$scope.jacht.fotos = $scope.jacht.fotos -1;
	 	setTimeout(function(){
		 	delete $scope.jacht.fhommels["foto-"+index];
		 	
		}, 500)
	 	/**/
  	}
  	
  	
  	/*
	* Check if user has internet
	* Uploads the foto's online 
	* Saves the data to the database
  	* cleares the memory
  	* changes the state 
  	*/ 
  	$scope.opslaan = function() {
    	
    	if(!$scope.online){
	    	var hideSheet2 = $ionicActionSheet.show({
		     buttons: [
		       { text: 'maak verbinding'}
		     ],
		    // destructiveText: 'Delete',
		     destructiveText: 'het lukt niet om nu verbinding te maken',
		     titleText: 'Om te kunnen opslaan moet je een werkende internetverbinding hebben. Loop naar een wifipunt of maak op een andere manier verbindng.',
		     cancelText: 'annuleren',
		     cancel: function() {
		          // add cancel code..
		        },
		     buttonClicked: function(index) {
		       console.log(index)
		       return true;
		     },
		     
		     destructiveButtonClicked: function(){
			    var confirmPopup = $ionicPopup.confirm({
				  title: 'Je gegevens gaan op deze manier helaas verloren.', // String. The title of the popup.
				  //cssClass: '', // String, The custom CSS class name
				  subTitle: 'Weet je zeker dat je af wilt sluiten?', // String (optional). The sub-title of the popup.
				  //template: '', // String (optional). The html template to place in the popup body.
				  //templateUrl: '', // String (optional). The URL of an html template to place in the popup   body.
				  cancelText: 'Nee', // String (default: 'Cancel'). The text of the Cancel button.
				  cancelType: 'button-default', // String (default: 'button-default'). The type of the Cancel button.
				  okText: 'Ja', // String (default: 'OK'). The text of the OK button.
				  okType: 'button-energized', // String (default: 'button-positive'). The type of the OK button.
				}).then(function(res) {
					if(res) {
						hideSheet2(); 
						$scope.jacht = undefined;
						if($scope.modal.isShown()){
							$scope.modal.hide();
						}
						$ionicHistory.nextViewOptions({
							disableBack: true
						});
						$state.go('app.kiesEenTaak', null, {
							location: 'replace'
						})
					}
					// else {
					  //     console.log('Nee');
					   // }
					});
				
				hideSheet2(); 
					 
		     },
		     cssClass : 'center'
    		});
	    }else{
			//save to database
			//also save empty jachts?
			Storage.saveJacht($scope);
	    }
	    
	    
  	};
  	
  	$scope.showActionSheet 	= function(){
			// Show the action sheet
		   var hideSheet = $ionicActionSheet.show({
		     buttons: [
		       { text: 'doorgaan'}
		     ],
		    // destructiveText: 'Delete',
		     destructiveText: 'stoppen',
		     titleText: 'Weet je zeker dat je wilt stoppen?',
		     cancelText: 'annuleren',
		     cancel: function() {
		          // add cancel code..
		        },
		     buttonClicked: function(index) {
		       console.log(index)
		       return true;
		     },
		     
		     destructiveButtonClicked: function(){
			      
			      	$scope.eindtijd		= new Date().getTime();
		            $scope.seconden 	= parseInt(($scope.eindtijd - $scope.jacht.starttijd) / 1000);
					$scope.minuten		= Math.floor($scope.seconden / 60);
					$scope.seconds 		= $scope.seconden - ($scope.minuten * 60);
					$scope.seconds_c	= ($scope.seconds < 10) ? "0"+$scope.seconds : $scope.seconds;
					$scope.totaaltijd	= $scope.minuten+":"+$scope.seconds_c;

			      
			      $ionicModal.fromTemplateUrl('templates/jagenOverzicht.html', {
		  				scope: $scope,
		  				animation: 'slide-in-up'
	  				}).then(function(modal) {
	  					$scope.modal = modal;
	  					$scope.modal.show();	
	  					hideSheet();	  					
	  				});
		     },
		     cssClass : 'center'
    		});
	}    

	$scope.$on('modal.hidden', function() {
		// Execute action
		$scope.modal.remove();
	});
	
  	  	
  	
}])
//TODO makethis optional if usingcomputer instead of mobile
.controller('fotoCtrl', ['$scope', '$stateParams', '$log', '$filter', 'Upload', 'Storage', 
function ($scope, $stateParams, $log, $filter, Upload, Storage) {
	
	var vm = this;
    vm.config = {
      delay: 0,
      shots: 3,     
      outputHeight: 480,
	  outputWidth : 640,
      //overlay: "img/foto_bg.png",
      enable_flash : false,
      shutterUrl: 'sound/shutter.mp3',
      iosPlaceholderText:    '>Klik hier om een foto te maken.'
	  //user_callback: function(){alert(12)},    
	  //user_canvas: null  
    };

    vm.showButtons 			= false;
    vm.captureButtonEnable 	= false;
    vm.progress 			= 0;
	
	vm.onCaptureComplete = function(src) {
		var el = document.getElementById('result');
		var img = document.createElement('img');
		img.src = src;
		img.width = 80;
		img.height = 60;
		el.appendChild(img);
		//angular.element(document.getElementById('debug')).html(s);
		
		$scope.filename = $scope.userData.school.post_name+"/"+$scope.userData.user_nicename+"-"+$filter('date')(new Date(), 'yyyyMMdd-HHmmss')+".png";
		
		$scope.jacht.fhommels[$scope.jacht.fotos] = {"time":new Date().getTime(), "position": {"latitude":$scope.position.coords.latitude, "longitude":$scope.position.coords.longitude}, "foto": $scope.filename}
		//console.log($scope.jacht.fhommels);
		$scope.jacht.fotos++;
		$scope.disabled		= false;
		Storage.saveImage($scope, src);
	}
	/*
	vm.onCaptureCompleteS = function(src) {
      $log.log(src);
      vm.progress = 100;
      var el = document.getElementById('result');
      var img = document.createElement('img');
      img.src = src;
      img.width = 240;//window.width? when testing
      img.height = 180;//window.height?
      el.appendChild(img);
    };
    */
    vm.onError = function(err) {
      $log.error('webcamController.onError : ', err);
      vm.showButtons = false;
    };
    
    vm.onLoad = function() {
      $log.info('webcamController.onLoad');
      vm.showButtons = true;
    };
    
    vm.onLive = function() {
      $log.info('424 webcamController.onLive');
      vm.captureButtonEnable = true;
    };
    
    //deze moeten ook herschreven worden
    vm.onCaptureProgress = function(src, progress) {
      vm.progress = progress;
      var result = {
        src: src,
        progress: progress
      }
      var el = document.getElementById('result');
      var img = document.createElement('img');
      img.src = src;
      img.width = 240;
      img.height = 180;
      el.appendChild(img);
      $log.info('webcamController.onCaptureProgress : ', result);
    };
    
    vm.capture = function() {
      $scope.$broadcast('ngWebcam_capture'); 
    };
    
    vm.on = function() {
      $scope.$broadcast('ngWebcam_on');
    };
    
    vm.off = function() {
      $scope.$broadcast('ngWebcam_off');
      vm.captureButtonEnable = false;
    };
	
	
	
	
}])
   
.controller('jagenOverzichtCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {
	//console.log("controller jachtover")
	//console.log($scope);
	/*
		$scope.jacht.eindtijd		= new Date().getTime();
			     Storage.saveLocalItem("hommeljacht",$scope.jacht);
				 $state.go('app.tellenOverzicht');
	*/

}])
   
.controller('fotoNemenCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('fotoOpslaanCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('alleenGeteldCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('hommelfotoAanpassenCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
 