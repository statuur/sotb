var CACHE_NAME = 'v1';
var baseUrl = "http://localhost";
var cacheFirstFiles = [
        '/img/c8MmMw3bTCGIc4upVdpQ_hommel-003.jpg',
		'/img/eVCDPJCFTsKYgRyge71Z_hommel-002.jpg',
		'/img/foto_bg.png',
		'/img/fRD5H9IS27kJZdPJQVLA_hommel-002.jpg',
		'/img/hommel-groot.jpg',
		'/img/hommel-jagen.jpg',
		'/img/hommel.jpg',
		'/img/hommeljacht.svg',
		'/img/hommelkast.svg',
		'/img/metpollen.svg',
		'/img/metpollen1.svg',
		'/img/s6zragGSDGpYV7JkLqHT_hommel-001.jpg',
		'/img/sotb-icon.png',
		'/img/sotb-icon.svg',
		'/img/WCskGjtRkeEc81e2shJv_take_photo_ios.jpg',
		'/img/zonderpollen.svg',
		'/img/zonderpollen1.svg',
        'img/icons/android-chrome-192x192.png',
		'/img/icons/android-chrome-192x192.png',
		'/img/icons/android-chrome-512x512.png',
		'/img/icons/apple-touch-icon.png',
		'/img/icons/favicon-16x16.png',
		'/img/icons/favicon-32x32.png',
		'/img/icons/favicon.ico',
		'/img/icons/mstile-70x70.png',
		'/img/icons/mstile-144x144.png',
		'/img/icons/mstile-150x150.png',
		'/img/icons/safari-pinned-tab.svg',
		'/css/ionic.app.css',
        'css/styles.app.css',
        'css/fonts/CalvertMTStd-Bold.ttf',
		'/css/fonts/CalvertMTStd-Bold.woff',
		'/css/fonts/CalvertMTStd-Light.ttf',
		'/css/fonts/CalvertMTStd-Light.woff',
		'/css/fonts/CalvertMTStd.ttf',
		'/css/fonts/CalvertMTStd.woff',
        'lib/ionic/js/ionic.bundle.min.js',
        '/templates/jagenOverzicht.html',
		'/templates/jagenUpload.html',
		'/templates/jagen.html',
		'/templates/test.html',
		'/templates/tellenOverzicht.html',
		'/templates/hommeljacht.html',
		'/templates/tellen.html',
		'/templates/tellenWeer.html',
		'/templates/tellenWind.html',
		'/templates/jagenWind.html',
		'/templates/jagenWeer.html',
		'/templates/kiesEenTaak.html',
		'/templates/welkom.html',
		'/templates/hommelkast.html',
		'/templates/inloggen.html',
		'/templates/menu.html',
		'/templates/foto.html',
		'/templates/telAlleHommels.html',
		'/templates/tellen-aanpassen.html',
		'/templates/signup.html',
		'/templates/opties.html',
		'/templates/fotoOpslaan.html',
		'/templates/alleenGeteld.html',
		'/templates/offline.html',
		'/index.html'
		
      ];

var networkFirstFiles = [
  		'/templates/jagenUpload.html',
  		'js/app.js',
		'/js/controllers.js',
		'/js/routes.js',
		'/js/directives.js',
		'/js/services.js',
		'/js/angular-gettext.min.js',
		'/bower_components/webcamjs/webcam.js',
		'/bower_components/ngVibrant/dist/angular-vibrant.min.js',
		'/lib/webcam/ng-webcam.js',
		'/lib/upload/ng-file-upload.min.js',
		'/bower_components/ngmap/build/scripts/ng-map.min.js',

];


var cacheFiles = cacheFirstFiles.concat(networkFirstFiles);


self.addEventListener("install", function(event) {
 // console.log('WORKER: install event in progress.');
  event.waitUntil(
    caches
     .open(CACHE_NAME)
      .then(function(cache) {
        return cache.addAll(cacheFiles);
      })
      .then(function() {
    //    console.log('WORKER: install cachefiles completed');
      })
  );
});


self.addEventListener("activate", function(event) {
  /* Just like with the install event, event.waitUntil blocks activate on a promise.
     Activation will fail unless the promise is fulfilled.
  */
  //console.log('WORKER: activate event in progress.');

  event.waitUntil(
    caches
      /* This method returns a promise which will resolve to an array of available
         cache keys.
      */
      .keys()
      .then(function (keys) {
        // We return a promise that settles when all outdated caches are deleted.
        return Promise.all(
          keys
            .filter(function (key) {
              // Filter by keys that don't start with the latest version prefix.
              return !key.startsWith(CACHE_NAME);
            })
            .map(function (key) {
              /* Return a promise that's fulfilled
                 when each outdated cache is deleted.
              */
              return caches.delete(key);
            })
        );
      })
      .then(function() {
      //  console.log('WORKER: activate completed.');
      })
  );
});



self.addEventListener("fetch", function(event) {
  //console.log('WORKER: fetch event in progress.');
//TODO exclude iosocket request //socket.io/?EIO=3&transport=polling&t=MDKc204&sid=XbKi5xHrWgtaVfFqAAW4
  /* We should only cache GET requests, and deal with the rest of method in the
     client-side, by handling failed POST,PUT,PATCH,etc. requests.
  console.log(event.request.url.indexOf(baseUrl), event.request.url.indexOf('socket.io'));
  */
  
  if (event.request.method !== 'GET' || event.request.url.indexOf(baseUrl) === -1 || event.request.url.indexOf('socket.io') >= 0) {
	/* If we don't block the event as shown below, then the request will go to
       the network as usual.
    */
    console.log('WORKER: fetch event ignored.', event.request.method, event.request.url);
    return;
  }
  /* Similar to event.waitUntil in that it blocks the fetch event on a promise.
     Fulfillment result will be used as the response, and rejection will end in a
     HTTP response indicating failure.
  */
  event.respondWith(
    caches
      /* This method returns a promise that resolves to a cache entry matching
         the request. Once the promise is settled, we can then provide a response
         to the fetch request.
      */
      .match(event.request)
      .then(function(cached) {
        /* Even if the response is in our cache, we go to the network as well.
           This pattern is known for producing "eventually fresh" responses,
           where we return cached responses immediately, and meanwhile pull
           a network response and store that in the cache.
           Read more:
           https://ponyfoo.com/articles/progressive-networking-serviceworker
        */
        var networked = fetch(event.request)
          // We handle the network request with success and failure scenarios.
          .then(fetchedFromNetwork, unableToResolve)
          // We should catch errors on the fetchedFromNetwork handler as well.
          .catch(unableToResolve);

        /* We return the cached response immediately if there is one, and fall
           back to waiting on the network as usual.
        */
        //console.log('WORKER: fetch event', cached ? '(cached)' : '(network)', event.request.url);
        return cached || networked;

        function fetchedFromNetwork(response) {
          /* We copy the response before replying to the network request.
             This is the response that will be stored on the ServiceWorker cache.
          */
          var cacheCopy = response.clone();

         // console.log('WORKER: fetch response from network.', event.request.url);

          caches
            // We open a cache to store the response for this request.
            .open(CACHE_NAME)
            .then(function add(cache) {
              /* We store the response for this request. It'll later become
                 available to caches.match(event.request) calls, when looking
                 for cached responses.
              */
              cache.put(event.request, cacheCopy);
            })
            .then(function() {
            //  console.log('WORKER: fetch response stored in cache.', event.request.url);
            });

          // Return the response so that the promise is settled in fulfillment.
          return response;
        }

        /* When this method is called, it means we were unable to produce a response
           from either the cache or the network. This is our opportunity to produce
           a meaningful response even when all else fails. It's the last chance, so
           you probably want to display a "Service Unavailable" view or a generic
           error response.
        */
        function unableToResolve () {
          /* There's a couple of things we can do here.
             - Test the Accept header and then return one of the `offlineFundamentals`
               e.g: `return caches.match('/some/cached/image.png')`
             - You should also consider the origin. It's easier to decide what
               "unavailable" means for requests against your origins than for requests
               against a third party, such as an ad provider
             - Generate a Response programmaticaly, as shown below, and return that
          */

        //  console.log('WORKER: fetch request failed in both cache and network.');

          /* Here we're creating a response programmatically. The first parameter is the
             response body, and the second one defines the options for the response.
          */
          return caches.match('/templates/offline.html');
          /*
          return new Response('<h1>Service Unavailable</h1>', {
            status: 503,
            statusText: 'Service Unavailable',
            headers: new Headers({
              'Content-Type': 'text/html'
            })
          });
          */
        }
      })
  );
});