<?php
/*
       Controller name: Freon
	   Controller description: JSON registratiefuncties voor freonen in het inschrijfsysteem
       JSON Controller for JSON API WordPress Plugin to return posts in Timeline format for Verite Timeline WordPress Plugin
 
       Usage:
       https://www.slachtemarathon.nl/api/freon/status 
*/
//error_reporting(E_ERROR | E_PARSE);
//ini_set('display_errors', 1);

add_shortcode("vertaal", "vertaal" );
class JSON_API_Freon_Controller {
 	var $allowed_referers = array(
 				"http://localhost:9000/", 
 				"http://localhost:9000/tickets.html", 
 				"http://localhost:9000/tickets.html#!/form/inloggen", 
 				"https://www.slachtemarathon.nl/ticketservice/tickets.html", 
 				"http://localhost:9000/tickets.html#!/form/overzicht", 
 				"https://www.slachtemarathon.nl/ticketservice/tickets.html", 
 				"https://onwikkeling.slachtemarathon.nl/ticketservice/tickets.html"
 				);	//referer localhost weg bij livegang
 				
 	var $usermetafields 	= array(
					"Huisnummer" 	=> "huisnummer",
					"Straat"	 	=> "straat",
					"Postcode" 		=> "postcode",
					"Plaats" 		=> "plaats",
					"Telefoon"		=> "telefoon",
					"Geslacht"		=> "geslacht",
					"Geb_datum"		=> "geboortedatum",
					"first_name"	=> "voornaam",
					"last_name"		=> "achternaam"	
				);
				
	var	$unnessacery 		= array(
					"description", 
					"rich_editing",
					"comment_shortcuts",
					"admin_color",
					"use_ssl",
					"show_admin_bar_front",
					"dismissed_wp_pointers",
					"last_login",
					"description_en",
					"description_fy",									
					"Huisnr_Toev",
					"IBAN",
					"Inschr_datum",
					"land",
					"Telefoon_mobiel",
					"Toevoeging",
					"aantal_introducees",
					"Lidnr",
					"introducee",
					"nickname"	,
					"Betaalwijze",
					"Land"								
					);

 	var $dorpenquota 	= array(
					"Easterein"	=> 9, 
					"Tzum" 		=> 14, 
					"Tritzum"	=> 13, 
					"Kubaard"	=> 12,
					 "Wommels"	=> 11
					 );
					 
	var $slaapplekken	= array(
					"In een groepsaccommodatie" => "groepsaccomodaties", 
					"Bij iemand thuis" 			=> "thuis", 
					"Festipi"					=> "festipi", 
					"In eigen tent"				=> "tenten", 
					"In eigen camper of caravan"=> "campers"
					);
					
					
	var $talen			= array(
							"nl_NL"=>"nl",
							"fy_NL"=>"fy",
							"en_US"=>"en"
						);
		
					
 	public function emailCheck(){
	 	global $wpdb;
	 	if(!isset($_POST['email'])){
		 	return array("status"=>"security error");
	 	}
	 	$exists = $wpdb->get_results("SELECT user_id FROM wp_usermeta where meta_key='verzend-email' and meta_value='{$_POST['email']}' ", OBJECT );
		
	 	//$exists  = get_user_by("email", $_POST['email']);
	 	//echo "SELECT user_id FROM wp_usermeta where meta_key='verzend-email' and meta_value='{$_POST['email']}' ";
	 	//print_r($exists);
	 	return array("exists" => ($exists[0]->user_id!="")?1:0);
 	}
 	
public function voorkeurenOpslaan(){
	global $wpdb;
	/*$_POST = json_decode('{"status":"ok","reg":{"ID":"5","user_login":"testpersoon1","user_email":"statuur1@gmail.com","role":"freon","email":"statuur1@gmail.com","voornaam":"Bastiaan1","achternaam":"Blaauw1","locale":"","last_login":"1506505954","shirts":"[{\"gender\":\"v\",\"size\":\"l\"},{\"gender\":\"m\",\"size\":\"s\"},{\"size\":\"xxl\",\"gender\":\"m\"}]","hoodies":"[{\"gender\":\"v\",\"size\":\"m\"},{\"gender\":\"v\",\"size\":\"s\"}]","order_id":"","paymentID":"","tussenvoegsels":"de1","straat":"Reidkampen1","huisnummer":"241","postcode":"8921 Bs","plaats":"Easterein1","telefoon":"0566-601231","geslacht":"vrouw","geboortedatum":"1970-01-01T00:00:00.000Z","deelgenomen":"4","starttijd":"09:00-10:00","vervoer":"Ik word gebracht","opstapplek":"Leeuwarden","slapen":"In één van de út-en-thús-dorpen langs de route","dorp":"Wommels","soortovernachting":"Bij iemand thuis","slaapplek":"Ik regel het zelf","extrapersonen":"","bagage":"Ja, graag verzorgen","taal":"nl_NL","wachtwoord":"tpersoon_1234*","verzendemail":"","mailSent":"1506006180","aantalkaarten":"1","nachtenslapen":"","tentcaravancamper":""},"betaald":"1506392400","introducees":[{"pwijzigen":"edit","geboortedatum":"2017-09-27T09:57:40.381Z"},{"pwijzigen":"edit","geboortedatum":"1970-01-01T00:00:00.000Z","soortovernachting":""},{"pwijzigen":"edit","geboortedatum":"1970-01-01T00:00:00.000Z","soortovernachting":""}],"kaarten":1,"shirts":{},"hoodies":{},"user":"","password":"","opslaan":false}');
	$_POST = (array)$_POST;
	$_POST['reg'] = (array)$_POST['reg'];
	$user 			= get_userdata($_POST['reg']['ID']);
	*/
	
	if(!$user){
		$error = array("status"=>"error", "error"=>"Gebruiker bestaat niet");
		return $error;
	}
	print_r($_POST);
	die();
	$user = $user->data;
	
	$usermeta = get_user_meta($user->ID);
				foreach($usermeta as $key => $value){
					$v 				= $value[0];
					//filter out the unnessacery metadata
					if(stripos($key, "_")===0 || stripos($key, "wp")===0 || in_array($key, $this->unnessacery)){
						unset($usermeta->$key);	
						continue;										
					}
					
					if(in_array($key, array_keys($this->usermetafields))){
					 	$key = $this->usermetafields[$key];
					}
					$user->$key = $value[0];
				}		
	
	/******* UPDATE FREON *******/
		$this->userdata	= array( 
		"ID"			=> $_POST['reg']['ID'],
		"user_email" 	=> $_POST['reg']['email'], 
		"first_name" 	=> $_POST['reg']['voornaam'],
		"user_nicename"	=> ($_POST['reg']['tussenvoegsels']!="") ? $_POST['reg']['voornaam']." ".$_POST['reg']['tussenvoegsels']." ".$_POST['reg']['achternaam'] : $_POST['reg']['voornaam']." ".$_POST['reg']['achternaam'],
		"last_name"		=> $_POST['reg']['achternaam']	
		);
	//$user_id = wp_update_user($this->userdata);
	
	/***** UPDATE FREON METADATA *****/
		$this->iusermetafields 	= array(//omdraaien
			"huisnummer" 	=> "Huisnummer",
			"straat"	 	=> "Straat",
			"postcode" 		=> "Postcode",
			"plaats" 		=> "Plaats",
			"telefoon"		=> "Telefoon",
			"geslacht"		=> "Geslacht",
			"geboortedatum"	=> "Geb_datum"			
		);
		
		$_POST['reg']['verzendemail'] = $_POST['email'];
		foreach($_POST['reg'] as $key=>$value){
			if($key=="ID" || $key=="hoodies" || $key=="order_id" || $key=="voornaam" || $key=="achternaam" || $key=="email" || $key=="shirts" || $key=="email2")continue;//SKIP USERFIELDS
			
			if($key=="geboortedatum"){
				$datumtijd = strtotime($value);//+3600;
				$value	   = date("d-m-Y", $datumtijd);//1958-03-09 00:00:00
			}
			
			if(in_array($key, array_keys($this->iusermetafields))){
				$key = $this->iusermetafields[$key];
			}
			
			if($value!=""){
				//echo "update_user_meta(".$_POST['reg']['ID'].", ".$key.", ".$value.";\n";	
				update_user_meta($_POST['reg']['ID'], $key, $value);	
			}
				
		}
	
	
	/******** update quota ***********/
	
	if($user->dorp!=$_POST['reg']['dorp']){
		//van de nieuwe keuze komt er 1 bij
		$bdorpenID 			= $this->dorpenquota[$_POST['reg']['dorp']];
		$bsoortovernachting	= $this->slaapplekken[$_POST['reg']['soortovernachting']];
		$bgereserveerd		= get_field($bsoortovernachting, $bdorpenID);
		update_post_meta($bdorpenID, $bsoortovernachting, ($bgereserveerd+1));
		//echo "update_post_meta(".$bdorpenID.", ".$bsoortovernachting.", ".($bgereserveerd+1).");\n";
		$wpdb->query("update wp_starttijden set inschrijvingen=inschrijvingen+1 WHERE selector='{$_POST['reg']['starttijd']}'", OBJECT );
		
		//van de bestaande keuze gaat er 1 af 		
		$ndorpenID 			= $this->dorpenquota[$user->dorp];
		$nsoortovernachting	= $this->slaapplekken[$user->soortovernachting];
		$ngereserveerd		= get_field($nsoortovernachting, $ndorpenID);
		$wpdb->query("update wp_starttijden set inschrijvingen=inschrijvingen-1 WHERE selector='{$user->starttijd}'", OBJECT );
		update_post_meta($ndorpenID, $nsoortovernachting, ($ngereserveerd-1));
		
		

		
	}
	return array();
}

/*
 * Nadat de gebruikers op de knop akkoord en betalen heeft geklikt worden de gegevens
 * geupdate en de gekoppelde meelopers als user met rol 'meeloper' toegevoegd.
*/ 
 	public function update() {
		global $wpdb;
		
		/******* USER EXISTS *******/
		$user 		= get_userdata($_POST['reg']['ID']);
		if(!$user){
			$error = array("status"=>"error", "error"=>"Gebruiker bestaat niet");
			return $error;
		}
		update_user_meta($_POST['reg']['ID'], "shirts", json_encode($_POST['shirts']));
		update_user_meta($_POST['reg']['ID'], "hoodies", json_encode($_POST['hoodies'])); 
		update_user_meta($_POST['reg']['ID'], "taal", $_POST['taal']); 
		

		/******* SHIRTS EN HOODIES OPSLAAN IN DATABASE *******/
		if(sizeof($_POST['shirts']) > 0){
			for($i=0;$i < sizeof($_POST['shirts']); $i++){
				$n = $i+1;
				//$shirtsfield .= "Model: {$_POST['shirts'][$i]['gender']}, Maat: {$_POST['shirts'][$i]['size']}\n";//<--moet beter
				$query= "select modelID from modellen where model='{$_POST['shirts'][$i]['gender']}' and maat='{$_POST['shirts'][$i]['size']}' and product='Trainingsshirt'";
				$model = $wpdb->get_results("select * from modellen where model='{$_POST['shirts'][$i]['gender']}' and maat='{$_POST['shirts'][$i]['size']}' and product='Trainingsshirt'", OBJECT );
				//print_r($model[0]->modelID);
				$wpdb->query("insert into modellen_deelnemers values(null, {$_POST['reg']['ID']}, {$model[0]->modelID}, '{$model[0]->model} - {$model[0]->maat}')", OBJECT );
				}	
		}
		
		/******* HOODIES OPSLAAN IN DATABASE *******/
		if(sizeof($_POST['hoodies']) > 0){
			for($i=0;$i < sizeof($_POST['hoodies']); $i++){
				$n = $i+1;
				//$shirtsfield .= "Model: {$_POST['shirts'][$i]['gender']}, Maat: {$_POST['shirts'][$i]['size']}\n";//<--moet beter
				$query= "select modelID from modellen where model='{$_POST['shirts'][$i]['gender']}' and maat='{$_POST['hoodies'][$i]['size']}' and product='Hoodie'";
				$model = $wpdb->get_results("select * from modellen where model='{$_POST['hoodies'][$i]['gender']}' and maat='{$_POST['hoodies'][$i]['size']}' and product='Hoodie'", OBJECT );
				$wpdb->query("insert into modellen_deelnemers values(null, {$_POST['reg']['ID']}, {$model[0]->modelID}, '{$model[0]->model} - {$model[0]->maat}')", OBJECT );
				}	
		}
		
		/******* UPDATE FREON *******/
		$this->userdata	= array( 
		"ID"			=> $_POST['reg']['ID'],
		"user_email" 	=> $_POST['reg']['email'], 
		"first_name" 	=> $_POST['reg']['voornaam'],
		"user_nicename"	=> ($_POST['reg']['tussenvoegsels']!="") ? $_POST['reg']['voornaam']." ".$_POST['reg']['tussenvoegsels']." ".$_POST['reg']['achternaam'] : $_POST['reg']['voornaam']." ".$_POST['reg']['achternaam'],
		"last_name"		=> $_POST['reg']['achternaam']	
		);
		$user_id = wp_update_user($this->userdata);
		
		/***** UPDATE FREON METADATA *****/
		$this->iusermetafields 	= array(//omdraaien
			"huisnummer" 	=> "Huisnummer",
			"straat"	 	=> "Straat",
			"postcode" 		=> "Postcode",
			"plaats" 		=> "Plaats",
			"telefoon"		=> "Telefoon",
			"geslacht"		=> "Geslacht",
			"geboortedatum"	=> "Geb_datum"			
		);
		
		$_POST['reg']['verzendemail'] = $_POST['email'];
		foreach($_POST['reg'] as $key=>$value){
			if($key=="ID" || $key=="voornaam" || $key=="achternaam" || $key=="email" || $key=="shirts" || $key=="email2")continue;//SKIP USERFIELDS
			
			if($key=="geboortedatum"){
				$datumtijd = strtotime($value);//+3600;
				$value	   = date("d-m-Y", $datumtijd);//1958-03-09 00:00:00
			}
			
			if(in_array($key, array_keys($this->iusermetafields))){
				$key = $this->iusermetafields[$key];
			}
			
			//echo "update_user_meta(".$_POST['reg']['userID'].", ".$key.", ".$value.";\n";	
			update_user_meta($_POST['reg']['ID'], $key, $value);		
		}
			
			if($_POST['aanpassen']!=1){
				update_user_meta($_POST['reg']['ID'], "betaald", 0);
			}
			
		/**** INSERT INTRODUCEES ****/
		if($_POST['kaarten'] > 1){
			$i_id =array();
			for($i=0; $i < $_POST['kaarten']-1; $i++){
				//WEGWEGWEG
				$_POST['introducees'][$i]['user_email'] = $_POST['introducees'][$i]['email'];
				$_POST['introducees'][$i]['verzendemail'] = $_POST['introducees'][$i]['email'];
				
				if($_POST['introducees'][$i]['email']=="") continue;//als checkup			
				$i_id[] = $this->addMeeloper($i, $_POST['reg']['ID']);
			}	
			//set meelopers in a cookie for $this->betaald();
			
			setcookie("meelopers", json_encode($i_id), time()+3600, "","", true);
			//add meelopers to freon
			update_user_meta($_POST['reg']['ID'], "meelopers", serialize($i_id));		
		
		}
		
		return array();
	}
 	/*
 * Nadat de gebruikers op de knop akkoord en betalen heeft geklikt worden de gegevens
 * geupdate en de gekoppelde meelopers als user met rol 'meeloper' toegevoegd.
*/ 
 	public function updateLoadtest() {
		global $wpdb;
		//if(!in_array($_SERVER['HTTP_REFERER'], $this->allowed_referers))return array("status","error: verkeerde referrer");
		//die;
		$userID = rand(6, 2801);
		$freond 		= get_user_by('ID', $userID);
		$usermeta = get_user_meta($freond->ID);
		$freon    = $freond->data;	
		//update_user_meta(3242, "freon", 1319);
		//die;	
			foreach($usermeta as $key=>$value){
				if($key=="ID" || stripos($key, "_")===0 || stripos($key, "wp")===0 || in_array($key, $this->unnessacery))continue;
				
				if($key=="verzend-email"){
					$key="verzendemail";
					$freon->user_email  = $value[0];
					//echo $value[0]." <\n";
					$freon->$key = $value[0];
				}else{
					$freon->$key = $value[0];
				}
				
			}
			$deelgenomen = rand(0,5);
			$deelgenomen = $deelgenomen." keer";
			
			$dorpen = array("Easterein","Wommels","Kubaard","Tritzum","Tzum");
			$dn1 = rand(0,4);
			$dn2 = rand(0,4);
			$dn3 = rand(0,4);
			$dorp1 = $dorpen[$dn1];
			$dorp2 = $dorpen[$dn2];
			$dorp3 = $dorpen[$dn3];
			
			$intro->user_login1 = wp_generate_password(18, false )."@pasuwemailadresaubaan.nl";
			$intro->user_login2 = wp_generate_password(18, false )."@pasuwemailadresaubaan.nl";
			
			$starttijden = array("06:30-07:00", "07:00-08:00" ,"08:00-09:00","09:00-10:00","10:00-11:00");
			$starttijd	 = $starttijden[$dn1];
			$starttijd2	 = $starttijden[$dn2];
			$starttijd3	 = $starttijden[$dn3];
			
			$overnachtingen = array("In een groepsaccommodatie","Bij iemand thuis", "Festipi", "In eigen tent", "In eigen camper of caravan");
			$overnachting1 = $overnachtingen[$dn3];
			$overnachting2 = $overnachtingen[$dn2];
			$overnachting3 = $overnachtingen[$dn1];
			
			$e1 = rand(1,5);
			$e2 = rand(1,5);
			$e3 = rand(1,5);
			
			$naamrand1 = rand(0,100);
			$naamrand2 = rand(0,100);
			$naamrand3 = rand(0,100);
			$naamrand4 = rand(0,100);
			
			
		$_POST = json_decode('{"status":"ok","reg":{"ID":"'.$userID.'","user_login":"'.$freon->user_login.'","user_email":"'.$freon->user_email.'","role":"freon","voornaam":"'.$freon->first_name.'","achternaam":"'.$freon->last_name.'","order_id":"","paymentID":"","tussenvoegsels":"de","straat":"Teststraat","huisnummer":"1","postcode":"'.$freon->postcode.'","plaats":"'.$freon->Plaats.'","telefoon":"'.$freon->Telefoon.'","geslacht":"man","geboortedatum":"'.$freon->Geb_datum.'","deelgenomen":"'.$deelgenomen.'","starttijd":"'.$starttijd.'","vervoer":"Met de Slachtemarathonbus","opstapplek":"Leeuwarden","slapen":"In één van út en thús dorpen langs de route","dorp":"'.$dorp1.'","soortovernachting":"'.$overnachting1.'","slaapplek":"Ik regel het zelf","extrapersonen":"'.$e1.'","taal":"nl_NL","wachtwoord":"'.$freon->wachtwoord.'","verzend-email":"'.$freon->verzendemail.'","freon":"1","sendEmail":"1","nachtenslapen":"Ik blijf 2 nachten slapen","tentcaravancamper":"Op vrijdag 31 augustus","email":"'.$freon->user_email.'","email2":"'.$freon->user_email.'","betaald":0,"bagage":"Ja, graag verzorgen"},"betaald":"0",
		"introducees":[
		
		{"pwijzigen":"edit","geboortedatum":"1999-07-03T23:00:00.000Z","starttijd":"'.$starttijd2.'","vervoer":"Met de Slachtemarathonbus","slapen":"In één van út en thús dorpen langs de route","dorp":"'.$dorp2.'","soortovernachting":"'.$overnachting1.'","slaapplek":"Ik regel het zelf","bagage":"Ja, graag verzorgen","user_email":"'.$intro->user_login1.'","role":"meeloper","locale":"","shirts":"[{\"gender\":\"v\",\"size\":\"xl\"}]","order_id":"","paymentID":"","straat":"Teststraat","huisnummer":"1","postcode":"8901BW","plaats":"Leeuwarden","telefoon":"0612345678","deelgenomen":"2 keer","opstapplek":"Franeker/Harlingen","taal":"","wachtwoord":"tpersoon_1234*","verzend-email":"'.$intro->user_login1.'","freon":"1","sendEmail":"1","nachtenslapen":"Ik blijf 1 nacht slapen","tentcaravancamper":"Op vrijdag 31 augustus","email":"'.$intro->user_login1.'","email2":"'.$intro->user_login1.'","voornaam":"Lotus Ilse'.$naamrand1.'","achternaam":"Blaauw'.$naamrand2.'","geslacht":"vrouw"},
		
		{"pwijzigen":"edit","geboortedatum":"1999-06-20T23:00:00.000Z","starttijd":"'.$starttijd2.'","vervoer":"Met de Slachtemarathonbus","slapen":"In één van út en thús dorpen langs de route","dorp":"'.$dorp3.'","soortovernachting":"'.$overnachting1.'","slaapplek":"Ik regel het zelf","bagage":"Ja, graag verzorgen","user_email":"'.$intro->user_login2.'","role":"meeloper","locale":"","shirts":"[{\"gender\":\"m\",\"size\":\"m\"}]","order_id":"","paymentID":"","straat":"Teststraat","huisnummer":"1","postcode":"8901BW","plaats":"Leeuwarden","telefoon":"0612345678","deelgenomen":"2 keer","opstapplek":"Heerenveen","taal":"","wachtwoord":"tpersoon_1234*","verzend-email":"'.$intro->user_login2.'","freon":"1","sendEmail":"1","nachtenslapen":"Ik blijf 2 nachten slapen","tentcaravancamper":"Op vrijdag 31 augustus","email":"'.$intro->user_login2.'","email2":"'.$intro->user_login2.'","voornaam":"Nouria Rosa Iline'.$naamrand3.'","achternaam":"Blaauw'.$naamrand4.'","geslacht":"vrouw"},{"pwijzigen":"edit","geboortedatum":"1970-01-01T00:00:00.000Z","opstapplek":"Heerenveen"},{"opstapplek":"Franeker/Harlingen"}],"kaarten":3,"user":"testpersoon1","shirts":{},"password":"tpersoon_1234*","taal":"nl_NL","opslaan":false,"extrapersonen":'.$e3.',"totaalbedrag":"363.00", "shirts":"[{\"gender\":\"m\",\"size\":\"m\"}, {\"gender\":\"v\",\"size\":\"xl\"}]"}
');
		
		

		$_POST = (array) $_POST;
 		$_POST['reg']= (array) $_POST['reg'];
 		$_POST['shirts']= (array) json_decode($_POST['shirts']);
 		
 		$_POST['reg']['tussenvoegsels'] = ($_POST['reg']['tussenvoegsels']=="") ? "" : " ".$_POST['reg']['tussenvoegsels']." ";
 		$wpdb->query("update wp_starttijden set inschrijvingen=inschrijvingen+1 WHERE selector='{$_POST['reg']['starttijd']}'", OBJECT );
 		
 		$dorpenID 			= $this->dorpenquota[$_POST['reg']['dorp']];
		$soortovernachting	= $this->slaapplekken[$_POST['reg']['soortovernachting']];
		$gereserveerd		= get_field($soortovernachting, $dorpenID);
		update_post_meta($dorpenID, $soortovernachting, $gereserveerd+1);
 		
 		
 		
		
				/******* USER EXISTS *******/
		$user 		= get_userdata($_POST['reg']['ID']);
		
		if(!$user){
			$error = array("status"=>"error", "error"=>"Gebruiker bestaat niet");
			return $error;
		}
		
		update_user_meta($_POST['reg']['ID'], "shirts", json_encode($_POST['shirts']));
		update_user_meta($_POST['reg']['ID'], "hoodies", json_encode($_POST['hoodies'])); 
		update_user_meta($_POST['reg']['ID'], "taal", $_POST['taal']); 
		

		/******* SHIRTS OPSLAAN IN DATABASE *******/
		if(sizeof($_POST['shirts']) > 0){
			for($i=0; $i < sizeof($_POST['shirts']); $i++){
				//$n = $i+1;
				$_POST['shirts'][$i] = (array)$_POST['shirts'][$i];
	
				//$shirtsfield .= "Model: {$_POST['shirts'][$i]['gender']}, Maat: {$_POST['shirts'][$i]['size']}\n";//<--moet beter
				$model = $wpdb->get_results("select * from modellen where model='{$_POST['shirts'][$i]['gender']}' and maat='{$_POST['shirts'][$i]['size']}' and product='Trainingsshirt'", OBJECT );
				
				$wpdb->query("insert into modellen_deelnemers values(null, {$_POST['reg']['ID']}, {$model[0]->modelID}, '{$model[0]->model} - {$model[0]->maat}')", OBJECT );
				
				}	
		}
		
		/******* HOODIES OPSLAAN IN DATABASE *******/
		if(sizeof($_POST['hoodies']) > 0){
			for($i=0;$i < sizeof($_POST['hoodies']); $i++){
				$n = $i+1;
				$_POST['hoodies'][$i] = (array)$_POST['hoodies'][$i];
				$query= "select modelID from modellen where model='{$_POST['hoodies'][$i]['gender']}' and maat='{$_POST['hoodies'][$i]['size']}' and product='Hoodie'";
				$model = $wpdb->get_results("select * from modellen where model='{$_POST['hoodies'][$i]['gender']}' and maat='{$_POST['hoodies'][$i]['size']}' and product='Hoodie'", OBJECT );
				
				$wpdb->query("insert into modellen_deelnemers values(null, {$_POST['reg']['ID']}, {$model[0]->modelID}, '{$model[0]->model} - {$model[0]->maat}')", OBJECT );
				}	
		}
		
		/******* UPDATE FREON *******/
		$this->userdata	= array( 
		"ID"			=> $_POST['reg']['ID'],
		"user_email" 	=> $_POST['reg']['email'], 
		"first_name" 	=> $_POST['reg']['voornaam'],
		"user_nicename"	=> ($_POST['reg']['tussenvoegsels']!="") ? $_POST['reg']['voornaam']." ".$_POST['reg']['tussenvoegsels']." ".$_POST['reg']['achternaam'] : $_POST['reg']['voornaam']." ".$_POST['reg']['achternaam'],
		"last_name"		=> $_POST['reg']['achternaam']	
		);
		$user_id = wp_update_user($this->userdata);
		
		/***** UPDATE FREON METADATA *****/
		$this->iusermetafields 	= array(//omdraaien
			"huisnummer" 	=> "Huisnummer",
			"straat"	 	=> "Straat",
			"postcode" 		=> "Postcode",
			"plaats" 		=> "Plaats",
			"telefoon"		=> "Telefoon",
			"geslacht"		=> "Geslacht",
			"geboortedatum"	=> "Geb_datum"			
		);
		
		
		foreach($_POST['reg'] as $key=>$value){
			if($key=="ID" || $key=="voornaam" || $key=="achternaam" || $key=="email" || $key=="shirts" || $key=="email2")continue;//SKIP USERFIELDS
			
			if($key=="geboortedatum"){
				$datumtijd = strtotime($value);//+3600;
				$value	   = date("d-m-Y", $datumtijd);//1958-03-09 00:00:00
			}
			
			if(in_array($key, array_keys($this->iusermetafields))){
				$key = $this->iusermetafields[$key];
			}
			
			//echo "update_user_meta(".$_POST['reg']['userID'].", ".$key.", ".$value.";\n";	
			update_user_meta($_POST['reg']['ID'], $key, $value);		
		}
			update_user_meta($_POST['reg']['ID'], "betaald", 0);
			
		/**** INSERT INTRODUCEES ****/
		if($_POST['kaarten'] > 1){
			$i_id =array();
			for($i=0; $i < $_POST['kaarten']-1; $i++){
				$_POST['introducees'][$i] = (array) $_POST['introducees'][$i];
			
				$_POST['introducees'][$i]['user_email'] = $_POST['introducees'][$i]['email'];
				$_POST['introducees'][$i]['verzend-email'] = $_POST['introducees'][$i]['email'];
				$wpdb->query("update wp_starttijden set inschrijvingen=inschrijvingen+1 WHERE selector='{$_POST['introducees'][$i]['starttijd']}' ", OBJECT );
				if($_POST['introducees'][$i]['email']=="") continue;//als checkup			
				$i_id[] = $this->addMeeloper($i, $_POST['reg']['ID']);
			}	
			//set meelopers in a cookie for $this->betaald();
			
			setcookie("meelopers", json_encode($i_id), time()+3600, "","", true);
			//add meelopers to freon
			update_user_meta($_POST['reg']['ID'], "meelopers", serialize($i_id));		
		
		}
		
		return array();
	}
 	
 	private function addMeeloper($i, $freonID){
		
		$introducee 			= $_POST['introducees'][$i];		
		$introducee 			= (object)$introducee;
		
		$this->tussenvoegsel	= ($introducee->tussenvoegsels=="") ? " " : " ".trim($introducee->tussenvoegsels)." ";
		$this->email			= $introducee->email;
		$this->displayname		= trim($introducee->voornaam).$this->tussenvoegsel.trim($introducee->achternaam);
		
		$this->utv				= (trim($introducee->tussenvoegsels)=="") ? "" : trim($introducee->tussenvoegsels);		
		$this->username 		= strtolower(trim($introducee->voornaam).$this->utv.trim($introducee->achternaam));
		$this->username			= preg_replace('/\s+/', '-', $this->username);
		$this->username 		= $this->checkUser($this->username,1);
		$this->password 		= wp_generate_password( 12, false );
		$this->lastname			= ($introducee->tussenvoegsels=="") ? trim($this->lastname) : $introducee->tussenvoegsels." ".trim($this->lastname);
		$userdata = array(
			'user_login'  		=> $this->username,
			'user_pass'   		=> $this->password,
			'user_status'   	=> 1,
			'user_email'   		=> $this->email,//$freon->E_mail,
			'user_nicename'		=> $this->displayname,
			'first_name'		=> trim($introducee->voornaam),
			'last_name'			=> trim($introducee->achternaam),
			'display_name'		=> $this->displayname
			//'locale' eventueel voor de taal
		);
		//print_r($userdata );
		//CHECK USERNAME EXISTS OTHERWISE UPDATE INSTEAD OF INSERT!
		$exists			= username_exists($this->username);		
				
		$i_id 			= (!$exists) ? wp_insert_user( $userdata ) : wp_update_user( $userdata );		
		
		$user 			= new WP_User($i_id);
		$user->set_role( 'meeloper');//ook als de user bestaat als freon? update_user_meta("was_freon")
		
		if($exists && $_POST['aanpassen']!=1){
			update_user_meta($i_id, "wasfreon", 1);
		}
		
		update_user_meta($i_id, "wachtwoord", $this->password);		
		update_user_meta($i_id, "freon", $freonID);
		//echo $a.' update_user_meta('.$i_id.', "freon", '.$freonID.');\n';
		
		$this->addUserMeta($introducee, $i_id);
		
		return $i_id;
	}
	
	private function addUserMeta($introducee, $user_id){
		global $wpdb;
		//dit moet bij betaald.php
		//$wpdb->query("update wp_starttijden set inschrijvingen=inschrijvingen+1 WHERE selector='{$introducee['starttijd']}'", OBJECT );
		
		foreach($introducee as $key => $value){
			if($key=="ID" || $key=="voornaam" || $key=="freon" || $key=="achternaam" || $key=="tussenvoegsels" || $key=="email" || $key=="email2" || $key=="extrapersonen" || $key=="pwijzigen")continue;
			
			if($key=="geboortedatum"){
				$datumtijd = strtotime($value)+3600;
				$value	   = date("d-m-Y", $datumtijd);//1958-03-09 00:00:00
			}
			
			if(in_array($key, array_keys($this->iusermetafields))){
				$key = $this->iusermetafields[$key];
			}
			
		//echo "update_user_meta(".$user_id.", ".$key.", ".$value.";\n";	
			update_user_meta($user_id, $key, $value);
		}  	
	}
 	
	
	private function checkUser($username, $n){
		if ( username_exists( $username ) ){
			$username = $this->addOne($username, $n);
		}
	
		return $username;		
	}

	private function addOne($username, $n){
		$username = $username."1";
		$username = $this->checkUser( $username, $n+1);
		return $username;	
	}
	
/*
 * Deze functie wordt aangeroepen als de gebruiker naar de betaalomgeving gaat.		
*/
	public function storePayment(){
		global $wpdb;
		if(isset($_POST['userID']) && isset($_POST['paymentID']) ){
			update_user_meta($_POST['ID'], "paymentID", $_POST['paymentID']);
			update_user_meta($_POST['ID'], "order_id", $_POST['order_id']);
		}
		$wpdb->insert('wp_orders', array(
		    'ID' 			=> null,
		    'order_id' 		=> $_POST['order_id'],
		    'paymentID' 	=> $_POST['paymentID'],
		    'userID' 		=> $_POST['userID'],
		    'last_update'	=> date('Y-m-d H:i:s', time()),
		    'amount'		=> $_POST['amount'],
		    'status'		=> 0
			));
		return array();
	}		
/*
 * update a specific user that has payed 
*/ 	 	
 	public function betaald() {
 		global $wpdb;
 		
		//return $_POST;update freon locale
		//if(in_array($_SERVER['HTTP_REFERER'], $this->allowed_referers) &&  $_SERVER['REQUEST_METHOD'] === "POST" && $_POST['orderID']!=""){
			
			$dorpenID 			= $this->dorpenquota[$_POST['reg']['dorp']];
			$soortovernachting	= $this->slaapplekken[$_POST['reg']['soortovernachting']];
			$gereserveerd		= get_field($soortovernachting, $dorpenID);
			
			update_post_meta($dorpenID, $soortovernachting, $gereserveerd+1);
			update_user_meta($_POST['reg']['ID'], "betaald", time());
			
			$nu 		= date('Y-m-d H:i:s', time());
			$wpdb->update("wp_orders", array( 'status' => 1, 'last_update' => $nu),array('order_id'=>$_POST['orderID']));
			$wpdb->query("update wp_starttijden set inschrijvingen=inschrijvingen+1 WHERE selector='{$_POST['reg']['starttijd']}'", OBJECT );
			
			$this->freonEmailbevestiging();
			//die("oke");
			if($_POST["kaarten"] > 1){
				
				include("freonenEmail.php");
				$content 	= do_shortcode( $content );
		
				for($i = 0; $i < $_POST["kaarten"]-1; $i++){
				
				
				//print_r($soortovernachting);
				
				
				//$query ="update wp_starttijden set inschrijvingen=inschrijvingen+1 WHERE selector='{$_POST['introducees'][$i]['starttijd']}' ";
				$wpdb->query("update wp_starttijden set inschrijvingen=inschrijvingen+1 WHERE selector='{$_POST['introducees'][$i]['starttijd']}' ", OBJECT );
				
				$dorpenID 			= $this->dorpenquota[$_POST['introducees'][$i]['dorp']];
				$soortovernachting	= $this->slaapplekken[$_POST['introducees'][$i]['soortovernachting']];
				$gereserveerd		= get_field($soortovernachting, $dorpenID);
				//echo $dorpenID." - ".$soortovernachting." - ".($gereserveerd+1)."\n";
			
				update_post_meta($dorpenID, $soortovernachting, $gereserveerd+1);
				
				//$fp = fopen('meeloperdorpen.json', 'w');
				//fwrite($fp, "".$dorpenID.", ".$soortovernachting.", ".$gereserveerd."+1");
				//fclose($fp);
				//TODO: deze doet t nog niet.
				}
				//setcookie("meelopers", json_encode($i_id), time()+3600, "/ticketservice","", true);
				$meelopers = (isset($_COOKIE['meelopers'])) ? json_decode($_COOKIE['meelopers']) : "";
					foreach($meelopers as $meeloperID){
						$meeloperdata 	= get_user_by( 'ID', $meeloperID);
						if(!$meeloperdata){
							//$fp = fopen('missendemeelopers.txt', 'a+');
							//fwrite($fp, "meeloper ".$meeloperID." bestaat niet. functie Betaald() in freon;\n");
							//fclose($fp);
							
						}else{
							update_user_meta($meeloperID, "betaald", time());
							$meeloper    	= $meeloperdata->data;	
							$meelopermeta 	= get_user_meta($meeloperID);//moet single worden met username en wachtwoord en voornaam en achternaam
							//print_r($meelopermeta);
							foreach($meelopermeta as $key=>$value){
								if($key=="user_login")continue;
								$meeloper->$key = $value[0];
							}
						}
					}
				}
			//}else{
			//	die("Er zijn vraagtekens omtrent je afkomst.");
			//}
			
		return array();
		
	} 	
 	public function sendInlog(){
		global $wpdb;
		$this->taal		= $this->talen[$_POST['taal']];
		$user_ids 		= $wpdb->get_results("SELECT user_id FROM wp_usermeta where meta_key='verzend-email' and meta_value='{$_POST['email']}' ", OBJECT );
		
		foreach($user_ids as $user_id){
			$user_id = $user_id->user_id;			
			$freon = get_user_by("ID", $user_id);			
			
			if(!$freon)continue;
			$usermeta = get_user_meta($freon->ID);
			$freon    = $freon->data;	
			foreach($usermeta as $key=>$value){
				$freon->$key = $value[0];
			}
			$freon->user_email  = $_POST['email'];
			$mailsent 			= $this->generateLoginEmail($freon);
		}
		return array("mailsent"=>$mailsent);
	} 	
	
	/*
	 * Deze functie stuurt alle freonen een e-mail met login gegevens. 
	 * Moet direct oproepbaar zijn. Taal altijd NL; 	
 	*/
 	public function SendLoginEmails() {
 		
 		$this->taal		= "nl";
 		$this->freonen = get_users(array("number"=>2800, "role"=>"freon")); 
		//$this->freonen = get_users(array( 'search' => '5' )); 
		//include("freonenEmail.php");
		//$content 	= do_shortcode( $content );
		
		
		foreach($this->freonen as $freond){
			$usermeta = get_user_meta($freond->ID);
			$freon    = $freond->data;	
			foreach($usermeta as $key=>$value){
				if($key=="ID" || stripos($key, "_")===0 || stripos($key, "wp")===0 || in_array($key, $this->unnessacery))continue;
				
				if($key=="verzend-email"){
					$key="verzendemail";
					$freon->user_email  = $value[0];
					//echo $value[0]." <\n";
					$freon->$key = $value[0];
				}else{
					$freon->$key = $value[0];
				}
				
			}
			//$freon->user_email  = $value[0];			
			
			if($freon->sendEmail!=1 && $freon->verzendemail!=""){
				//print_r($usermeta);
				//$this->generateLoginEmail($freon);
				//echo "update_user_meta(".$freon->ID.", 'sendEmail', 1);\n";
				//print_r($freon);
				echo $freon->ID." - ".$freon->sendEmail." - ".$freon->verzendemail." << \n";		
			}
		}

	return array();
 	}
	
	private function generateLoginEmail($freon){
		//get the id
		//login email = 1328
		$this->trID 	= 1328;//pll_get_post("1328", $this->taal);
		
		require_once("freonenEmail.php");

		$this->subject 			= do_shortcode("[vertaal veld='emailonderwerp' id='".$this->trID."']");
 		$this->inloggegevens 	= do_shortcode("[vertaal veld='inloggegevens' id='".$this->trID."']");
 		$this->aanhef 			= do_shortcode("[vertaal veld='aanhef' id='".$this->trID."']");
 		
 		$this->gebruikersnaam 	= do_shortcode("[vertaal veld='gebruikersnaam' id='".$this->trID."']");
 		$this->wachtwoord		= do_shortcode("[vertaal veld='wachtwoord' id='".$this->trID."']");
 		$this->content 			= do_shortcode($content);
 		
		
	 	$inloggegevens = "
 		<hr style=\"margin:32px 0\">
 		<h3>Inloggegevens:</h3>		
		<div class=\"first-row\">Gebruikersnaam</div><div class=\"last-row\">".$freon->user_login."</div>
		<div class=\"first-row\">Wachtwoord</div><div class=\"last-row\">".$freon->wachtwoord."</div>
		<hr style=\"margin:32px 0\">
		";
	 	$freon->naam = ($freon->tussenvoegsels!="") ?  $freon->first_name." ".$freon->tussenvoegsels." ".$freon->last_name : $freon->first_name." ".$freon->last_name;
	 	
	 	$this->aanhef 	= preg_replace("/%name%/", $freon->naam, $this->aanhef);
	 	
	 	$this->content	= preg_replace("/-INLOGGEGEVENS-/", $inloggegevens, $this->content);
	 	$this->content 	= preg_replace("/-AANHEF-/", $this->aanhef, $this->content);
	 	$this->to		= $freon->user_email;//"bastiaan@statuur.nl";//"silvia@slachtemarathon.nl";//silvia@slachtemarathon.nl $freon->user_email;
		//$this->to = "jetze@destaatvancreatie.nl";
		$this->mailsent	= wp_mail($this->to, $this->subject, $this->content);
		return $this->mailsent;
	 	}
		
	public function objToArray($obj, &$arr){

    if(!is_object($obj) && !is_array($obj)){
        $arr = $obj;
        return $arr;
    }

    foreach ($obj as $key => $value)
    {
        if (!empty($value))
        {
            $arr[$key] = array();
            $this->objToArray($value, $arr[$key]);
        }
        else
        {
            $arr[$key] = $value;
        }
    }
    return $arr;
}
 	
 	public function freonEmailbevestiging() {
 		
 		$_POST['reg']['tussenvoegsels'] = ($_POST['reg']['tussenvoegsels']=="") ? "" : " ".$_POST['reg']['tussenvoegsels']." ";
 		$this->taal						= "nl_NL"; //($_POST['taal']!="") ? $_POST['taal'] : "nl_NL";
 		$this->staal					= "nl";//($taal=="nl_NL") ? "nl":"fy";
 		
 		$this->Pid 						= pll_get_post("1288", $this->staal);
 		
 		include("generatebestelling_{$this->taal}.php");
 		$aanhef 	= do_shortcode("[vertaal veld='aanhef' id='{$this->Pid}']");
 		$aanhef 	= $aanhef." ".$_POST['reg']['voornaam'];
 		include("emailbevestiging.php");
		//$to 		= "marcia@slachtemarathon.nl";
		//$to 		= "silvia@slachtemarathon.nl";
		//$to			= "bastiaan@statuur.nl";
		$to			= $_POST['reg']['email'];
		$subject 	= do_shortcode("[vertaal veld='emailonderwerp' id='{$this->Pid}']");
		$content 	= do_shortcode($content);
		$content 	= preg_replace("/-AANHEF-/", $aanhef, $content);
		$content 	= preg_replace("/-BESTELLING-/", $this->bestelling, $content);
		
		//echo $content;
		//die;
		
		//update_user_meta($_POST['reg']['ID'], "betaald", time());
		$mailsent	= wp_mail( $to, $subject, $content);
		//print_r($mailsent);
		//die();
		return $mailsent;
 	}
 	
 	
 	public function inloggen() {
		//if(in_array($_SERVER['HTTP_REFERER'], $this->allowed_referers) &&  $_SERVER['REQUEST_METHOD'] === "POST"){		
		 	
		 	$username	= $_POST['user'];
		 	$pass 		= $_POST['password'];
		 	$user 		= get_user_by('login', $username);
		 	$user->role = $user->roles[0];
		 	//print_r($user);
		 	//die();
		 	$formData   = (object) array();
		 	if ($user && wp_check_password($pass, $user->data->user_pass, $user->ID) ){
				unset($user->user_nicename, $user->user_pass, $user->user_url, $user->user_registered, $user->user_activation_key, $user->user_status, $user->display_name);
				
			
				$formData->reg = $user->data;
				
				$usermeta = get_user_meta($user->ID);
				foreach($usermeta as $key => $value){
					$v 				= $value[0];
					//filter out the unnessacery metadata
					if(stripos($key, "_")===0 || stripos($key, "wp")===0 || in_array($key, $this->unnessacery)){
						unset($usermeta->$key);	
						continue;										
					}
					
					if(in_array($key, array_keys($this->usermetafields))){
					 	$key = $this->usermetafields[$key];
					}
					 
					if($key=="meelopers" && $key!=""){ 
					 //add meelopers
					 	$value[0] 	= unserialize($value[0]);
					 	$meelopers	= $value[0]; 
					 	//print_r($meelopers);
					 	$formData->introducees = array();
					 	foreach( $meelopers as  $meeloperID){
							$meeloperd 		= get_user_by( 'ID', $meeloperID);
							unset($meeloperd->user_nicename, $meeloperd->user_pass, $meeloperd->user_url, $meeloperd->user_registered, $meeloperd->user_activation_key, $meeloperd->user_status, $meeloperd->display_name);
							
							$meeloperd->email = $meeloperd->user_email;
							//TODO checken of de juiste e-mailadressen bij de introducees komen.
							$meeloperdata	= $meeloperd->data;
							$meelopermeta 	= get_user_meta($meeloperID);
							
							foreach($meelopermeta as $k=>$val){
								if(stripos($k, "_")===0 || stripos($k, "wp")===0 || in_array($k, $unnessacery)) continue;
							
								if(in_array($k, array_keys($this->usermetafields))){
							 		$k = $this->usermetafields[$k];
								}
								$meeloperdata->$k = $val[0];
							}
						 $formData->introducees[] = $meeloperdata;
					 	}
					 	//print_r($formData->introducees);
					 continue;
					 }
					 
					 $this->rootkeys = array(
						 			"kaarten",
						 			//"shirts",
						 			"betaald",
						 			"Betaalwijze"
					 );
					 
					 if(in_array($key, $this->rootkeys)){					 						 	
					 	$formData->$key			= $v;
					 }else{
						$formData->reg->$key	= $v; 
					 }
				}	
				update_user_meta($formData->reg->ID, "last_login", time());
				$formData->kaarten	= sizeof($formData->introducees)+1;
				$formData->status = "ok";
				
			} else{
			   $formData = array("status"=>"gebruiker bestaat niet of wachtwoord onjuist");
			}	
		 	
		//}else{
			 //  $formData = array("status"=>"foute verwijzing");
		//}
		
		return $formData;
	}	
 	
}

function vertaal( $atts, $content = null){
		//$postID = 1288;
		$vertaal = get_field($atts['veld'], $atts['id']);
		$vertaal = htmlspecialchars_decode ($vertaal, ENT_QUOTES);
		return $vertaal;
	}
	

function wpdocs_set_html_mail_content_type() {
    return 'text/html';
}
add_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' ); 
?>