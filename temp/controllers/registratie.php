<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
/*
       Controller name: Registratie
	   Controller description: JSON registratiesysteem scholen


        JSON Controller for JSON API WordPress Plugin to return Dorpeninfo
 
        Usage:
        http://example.com/api/timeline/category_posts/?category_id=123&post_type=timeline&amount=10&main_post_id=456
 
        Default values:
        category_id = null
        post_type = 'post'
        amount = -1 (all posts)
        main_post_id = first post from query, ordered by date in ascending order (the main post should be an introduction to the timeline being viewed)
 
        Content values can be changed to support custom post meta for enhanced usage.
 
*/
 
class JSON_API_Registratie_Controller {
	public function register() {
		/*$inschrijvingen 		= count_users();	
		$total 					= $inschrijvingen['total_users'] - $inschrijvingen['avail_roles']['administrator'];
		$user_query 			= new WP_User_Query( array("number"=>40) );
		//print_r($inschrijvingen);
		$inschrijving_status 	= get_option("inschrijvingenstatus");
		$statussen   			= array("inschrijvingenstatus"=> $inschrijving_status, "freonen_totaal"=>$total);
		
		$fp 					= fopen('ticketservice/json/statussen.json', 'w');
 		fwrite($fp, json_encode($statussen));
 		fclose($fp); 
		
		return $statussen;
		*/
	} 
	
	public function login() {		
		//update_option("inschrijvingenstatus", $_POST['newstatus']);
		
		$user 		= get_user_by('email', $_POST['email']);
		
		if ($user && wp_check_password($_POST['password'], $user->data->user_pass, $user->ID) ){
			$user->data->first_name 	= get_user_meta($user->ID, "first_name", true);
			$user->data->last_name 		= get_user_meta($user->ID, "last_name", true);
			$schoolID			 		= get_user_meta($user->ID, "school", true);
			$user->data->school 		= get_post($schoolID);

			$r 					= $user->data;
		}else{
			$r = array("error" => 1);
		}
		return $r;
	}
	
	public function logout() {		
		//print_r($_POST);
		update_post_meta($_POST['dorpID'], "inschrijf_status", $_POST['newstatus']);
		return array();
	}
	public function test() {
	$school		= "wassenb";
	$filepath 	= "/home/statuur/public_html/sotb/uploads/";
	$leerling	= "pws";
	if (!is_dir($filepath."/".$school)){ 
		mkdir($filepath."/".$school, 0700); 
	}
	
	if (!is_dir($filepath."/".$school."/".$leerling)){ 
		mkdir($filepath."/".$school."/".$leerling, 0700); 
	}
	
		
	
	}	
	public function upload() {
		
		//$scope.filename = $scope.userData.school.post_name+"/"+$scope.userData.user_nicename+"/"+$filter('date')(new Date(), 'yyyyMMdd-HHmmss')+".png"
		
		// remove the part that we don't need from the provided image and decode it
		
		//$fp = fopen($filepath."/posted.txt", "w");
		//fwrite($fp, $_POST['filename']);//
		//fclose($fp);
		
		// Save the image in a defined path
		//name convention = userData->school->postname / userData->user_login / timestamp.png
		//name convention = userData->school->postname / userData->user_login_timestamp.png

		
		$data 		= base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['file']));
		$filepath 	= "/home/statuur/public_html/sotb/uploads"; // or image.jpg
		
		if(isset($_POST['filename'])){
			$parts 		= explode("/", $_POST['filename']);
			$school		= $parts[0];
			$leerling	= $parts[1];
			$filename	= $parts[2];
			if (!is_dir($filepath."/".$school)){ 
				mkdir($filepath."/".$school, 0700); 
			}
		
			if (!is_dir($filepath."/".$school."/".$leerling)){ 
				mkdir($filepath."/".$school."/".$leerling, 0700); 
			}
			
			file_put_contents($filepath."/".$school."/".$leerling."/".$filename,$data);
			//now the image had to be saved in the database;
			
			return "uploaded";
		}else{
			return "error";
		}
	}
			
}
 
?>