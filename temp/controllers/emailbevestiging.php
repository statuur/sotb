<?php 
$content = "<!doctype html>
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<title>[vertaal veld='emailonderwerp' id='{$this->Pid}']</title>
<meta charset=\"utf-8\" />
<style>
	
	html{
		font-family: Arial, Helvetica;
		font-weight: 400;
		margin:0px;
		padding:0px;
		color:#000;
	}
	
	body{
		margin:16px;
		padding:0px;
		font-size:16px;		
		line-height: 28px;			
		background:#fff;
		color:#000;
	}
	
	h1{
		font-size:25px;
		font-weight:700;
		color:#000;
		margin:18px 0px 0px 0px;		
	}
	
	h2{
		font-size:22px;
		font-weight:400;
		color:#e10916;
	}
	
	h3{
		font-size:18px;
		font-weight:400;
		color:#e10916;
		margin:8px 0px 8px 0px;
	}
	
	.right{
		text-align:right;
	}	
	
	.row15{
		display:inline-block;
		width:15%;
		font-weight:200;
		vertical-align:top;
	}
	
	.row70{
		display:inline-block;
		width:70%;
		font-weight:200;
		vertical-align:top;
	}
	
	.first-row{
		display:inline-block;
		width:45%;
		font-weight:200;
		vertical-align:top;
		margin-right:5%;
	}
	
	.last-row{
		display:inline-block;
		width:50%;
		max-width:50%;
		font-weight: 700;
		word-break: break-all;
	}
	
	a[href], .ii a[href],
	a:link,a:visited{
		color:#e10916!important;
		-webkit-transition: none!important;
		transition: none!important;
		-webkit-transition: background-color 0.4s ease-in-out, color 0.4s ease-in-out, border-color 0.8s ease-in-out!important;
		transition: background-color 0.4s ease-in-out, color 0.4s ease-in-out, border-color 0.8s ease-in-out!important;
	}
	
	a:hover,a:active{
		color:#000!important;
		
	}
	
	*, :after, :before {
	    -webkit-box-sizing: border-box; 
	    -moz-box-sizing: border-box;
	    box-sizing: border-box;
	}
	
	hr{
		border:none;
		border-top:1px solid #a5a5a5
	}
</style>
</head>
<body>
<div style=\"width:100%;height:100%;\">
	<div style=\"width:100%;max-width:550px;padding:0px 0px;margin:0px auto;background:#ededed;\">
		<a href=\"https://www.slachtemarathon.nl/\" style=\"display: inline-block; width:100%;text-align: center;\"><img style=\"margin:0px auto;\" src=\"https://www.slachtemarathon.nl/wp-content/uploads/2017/09/slachte-logo.png\" alt=\"Slachtemarathon\"></a>
		<div style=\"padding:20px;\"><h2>-AANHEF-,</h2>[vertaal veld='inleiding' id='{$this->Pid}']
			-BESTELLING-
		</div>
	</div>
</div>
</body>
</html>";
?>