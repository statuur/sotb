<?php
/*
       Controller name: Systeem
	   Controller description: JSON systeeminformatie


        JSON Controller for JSON API WordPress Plugin to return Dorpeninfo
 
        Usage:
        https://www.slachtemarathon.nl/api/systeem/dorpen
        
*/
 
class JSON_API_Systeem_Controller {
 		
 	public function basisinfo() {
	global $wpdb;
	 //TODO: content op basis van cookie taal angular
	//print_r($_GET);
	
	//print_r($basisinfo['modellen']);
 	
 	
	$locale 	= ($_GET['taal']!="") ? $_GET['taal'] : "nl_NL";
	setlocale("lc_all",$locale);
	
	$languagecodes = array("nl_NL" => "nl","fy_NL"=>"fy","en_US"=>"en");
	$taalcode 	= $languagecodes[$locale];
	$args = array(
		'posts_per_page'   => -1,
		'orderby'          => 'modified',
		'order'            => 'ASC',
		'post_type'        => 'dorp',
		'post_status'      => 'publish',
		'suppress_filters' => true,
		'lang'             => $taalcode//($_GET['taal']!="") ? $_GET['taal'] : "nl_NL"
	);
	//print_r($args);
	//die();
	$inschrijving_status 	= get_option("inschrijvingenstatus");
	$basisinfo 				= array("inschrijving_status"=>$inschrijving_status); 
	//$basisinfo['modellen']	= array();
	
	$basisinfo["Trainingsshirt"]['sizes'] 	= array();
	$basisinfo["Trainingsshirt"]['sizes']["v"] 	= array();
	$basisinfo["Trainingsshirt"]['sizes']["m"] 	= array();
	$basisinfo["Hoodie"]['sizes'] 			= array();
	//$basisinfo["Hoodie"]['sizes']["v"] 	= array();
	//$basisinfo["Hoodie"]['sizes']["m"] 	= array();
	$modellen 				= $wpdb->get_results("SELECT * from modellen order by modelID", OBJECT);
	
 	foreach($modellen as $model){
	 	//if(!is_array($basisinfo['modellen'][$model->product])) ? $basisinfo['modellen'][$model->product]=array() : "";
	 	if($model->product=="Trainingsshirt"){
	 	$basisinfo[$model->product]['sizes'][$model->model][] = $model->maat;
	 	}else{
		$basisinfo[$model->product]['sizes'][$model->maat] = $model->maat; 	
	 	}
	 	//echo $model->product;
 	}
 	
	$basisinfo['dorpen']	= array();
	$pdorpen  				= get_posts($args);
	
	$i = 0;
	foreach ($pdorpen as $post) : setup_postdata($post );
		$post->afbeelding 			= wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full')[0];
		$post->typering				= get_field("typering", $post->ID);
		$post->afstand				= get_field("afstand", $post->ID);
		
		//$link 					= avia_image_by_id(get_post_thumbnail_id(), 'large', 'url');
		//$post->afbeelding			= get_field("afbeelding", $post->ID)[url];
		
		//
		$translatedID 				= pll_get_post( $post->ID, "nl");
		
		/* CAPACITEITSCHECK */
		$post->uniek				= get_field("unieke_naam", $translatedID);
		$post->inschrijf_status		= get_field("inschrijf_status", $translatedID);		
		$post->groepsaccomodaties	= get_field("c_groepsaccomodaties", $translatedID) - get_field("groepsaccomodaties", $translatedID);
		$post->thuis				= get_field("c_thuis", $translatedID) - get_field("thuis", $translatedID);
		$post->festipi				= get_field("c_festipi", $translatedID) - get_field("festipi", $translatedID);
		$post->tenten				= get_field("c_tenten", $translatedID) - get_field("tenten", $translatedID);
		$post->campers				= get_field("c_campers", $translatedID) - get_field("campers", $translatedID);
			
		$basisinfo['dorpen'][$post->post_title] = $post;
 	endforeach;
 	//print_r($basisinfo['dorpen']);
 	$basisinfo['starttijden'] = $wpdb->get_results("SELECT selector, DATE_FORMAT(van, '%H:%i') as van, DATE_FORMAT(tot, '%H:%i') as tot, capaciteit, inschrijvingen FROM wp_starttijden", OBJECT);
 	
 		
 	$fp = fopen('ticketservice/json/basisinfo.json', 'w');
 	fwrite($fp, json_encode($basisinfo));
 	fclose($fp);
 		
 		return $basisinfo;
 	}
 	
 	public function basisinfoOUDWEG() {
	global $wpdb;
	 //TODO: content op basis van cookie taal angular
	//print_r($_GET);
	
	//print_r($basisinfo['modellen']);
 	
 	
	$locale 	= ($_GET['taal']!="") ? $_GET['taal'] : "nl_NL";
	setlocale("lc_all",$locale);
	
	$languagecodes = array("nl_NL" => "nl","fy_NL"=>"fy","en_US"=>"en");
	$taalcode 	= $languagecodes[$locale];
	$args = array(
		'posts_per_page'   => -1,
		'orderby'          => 'modified',
		'order'            => 'ASC',
		'post_type'        => 'dorp',
		'post_status'      => 'publish',
		'suppress_filters' => true,
		'lang'             => $taalcode//($_GET['taal']!="") ? $_GET['taal'] : "nl_NL"
	);
	//print_r($args);
	//die();
	$inschrijving_status 	= get_option("inschrijvingenstatus");
	$basisinfo 				= array("inschrijving_status"=>$inschrijving_status); 
	$basisinfo['modellen']	= array();
	$basisinfo['modellen']["Trainingsshirt"] = array();
	$basisinfo['modellen']["Hoodie"] = array();
	$modellen 				= $wpdb->get_results("SELECT * from modellen order by modelID", OBJECT);

 	foreach($modellen as $model){
	 	//if(!is_array($basisinfo['modellen'][$model->product])) ? $basisinfo['modellen'][$model->product]=array() : "";
	 	$basisinfo['modellen'][$model->product][] = $model;
	 	//echo $model->product;
 	}
 	
	$basisinfo['dorpen']	= array();
	$pdorpen  				= get_posts($args);
	
	$i = 0;
	foreach ($pdorpen as $post) : setup_postdata($post );
		$post->afbeelding 			= wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full')[0];
		$post->typering				= get_field("typering", $post->ID);
		$post->afstand				= get_field("afstand", $post->ID);
		
		//$link 					= avia_image_by_id(get_post_thumbnail_id(), 'large', 'url');
		//$post->afbeelding			= get_field("afbeelding", $post->ID)[url];
		
		//
		$translatedID 				= pll_get_post( $post->ID, "nl");
		
		/* CAPACITEITSCHECK */
		$post->uniek				= get_field("unieke_naam", $translatedID);
		$post->inschrijf_status		= get_field("inschrijf_status", $translatedID);		
		$post->groepsaccomodaties	= get_field("c_groepsaccomodaties", $translatedID) - get_field("groepsaccomodaties", $translatedID);
		$post->thuis				= get_field("c_thuis", $translatedID) - get_field("thuis", $translatedID);
		$post->festipi				= get_field("c_festipi", $translatedID) - get_field("festipi", $translatedID);
		$post->tenten				= get_field("c_tenten", $translatedID) - get_field("tenten", $translatedID);
		$post->campers				= get_field("c_campers", $translatedID) - get_field("campers", $translatedID);
			
		$basisinfo['dorpen'][$post->post_title] = $post;
 	endforeach;
 	//print_r($basisinfo['dorpen']);
 	$basisinfo['starttijden'] = $wpdb->get_results("SELECT selector, DATE_FORMAT(van, '%H:%i') as van, DATE_FORMAT(tot, '%H:%i') as tot, capaciteit, inschrijvingen FROM wp_starttijden", OBJECT);
 	
 		
 	$fp = fopen('ticketservice/json/basisinfo.json', 'w');
 	fwrite($fp, json_encode($basisinfo));
 	fclose($fp);
 		
 		return $basisinfo;
 	}
 		
 	public function status() {
 		$inschrijving_status 	= get_option("inschrijvingenstatus");
 		return array("inschrijving_status"=>$inschrijving_status);	
 	}	
 	
 	public function aantalKaarten() {
 		$aantalkaarten 	= get_option("aantalkaarten");
 		$nieuwaantal	= $aantalkaarten + $_POST['aantalkaarten'];
 		update_option("aantalkaarten", $nieuwaantal);
 		return array("aantalkaarten"=>$nieuwaantal);	
 	}		
      
	  
}
 
?>