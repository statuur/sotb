'use strict';

var gulp        		= require('gulp');
var browserSync 		= require('browser-sync').create();


var sass 				= require('gulp-sass');
var minifyCss 			= require('gulp-minify-css');


//var gettext 			= require('gulp-angular-gettext');
//var reload      		= browserSync.reload;
//var sassBuild 			= require('ionic-gulp-sass-build');

//var sass 		= require('gulp-sass');


gulp.task('sass', function(done) {
  gulp.src('./sass/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./css/'))
    .on('end', done);
});



/*

var paths = {
  sass: ['./sass/*.scss']
};

gulp.task('default', ['sass']);



gulp.task('sass', function(done) {
  gulp.src('./sass/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});	
	
gulp.task('sass', sassBuild);

gulp.task('sass', function(){
  return sassBuild({
    dest: './css',
    sassOptions: {
      includePaths: [
        './sass/ionic.app.scss'
      ]
    },
    autoprefixerOptions :{
  browsers: [
    'last 2 versions',
    'iOS >= 7',
    'Android >= 4',
    'Explorer >= 10',
    'ExplorerMobile >= 11'
  ],
  cascade: false
	},
	onError: function(err) { console.error(err.message); this.emit('end'); }


  });
});
	
	
gulp.task('sass', function () {
  return gulp.src('./sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./sass/*.scss', ['sass']);
});
*/

//gulp.task('pot', function () {
  //  return gulp.src(['templates/*.html', 'js/*.js', 'lib/**/*.js'])
    //    .pipe(gettext.extract('template.pot', {
     //       // options to pass to angular-gettext-tools... 
      //  }))
        //.pipe(gulp.dest('po/'));
//});
 
//gulp.task('translations', function () {
 //   return gulp.src('po/**/*.po')
   //     .pipe(gettext.compile({
      //      // options to pass to angular-gettext-tools... 
     //       format: 'json'
      //  }))
      //  .pipe(gulp.dest('translations/'));
//});

// Save a reference to the `reload` method

// Watch scss AND html files, doing different things with each.
gulp.task('serve', function () {

    // Serve files from the root of this project
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch("*.html").on("change", reload);
    gulp.watch("./templates/*.html").on("change", reload);
    gulp.watch("./js/*.js").on("change", reload);
    gulp.watch("./lib/**/*.js").on("change", reload);
    //gulp.watch("./sass/*.scss").on("change", reload);
    gulp.watch("./css/*.css").on("change", reload);
});

gulp.task('default', ['sass', 'serve']);