var VERSION = 'v1';
var cacheFirstFiles = [
        '/img/c8MmMw3bTCGIc4upVdpQ_hommel-003.jpg',
		'/img/eVCDPJCFTsKYgRyge71Z_hommel-002.jpg',
		'/img/foto_bg.png',
		'/img/fRD5H9IS27kJZdPJQVLA_hommel-002.jpg',
		'/img/hommel-groot.jpg',
		'/img/hommel-jagen.jpg',
		'/img/hommel.jpg',
		'/img/hommeljacht.svg',
		'/img/hommelkast.svg',
		'/img/metpollen.svg',
		'/img/metpollen1.svg',
		'/img/s6zragGSDGpYV7JkLqHT_hommel-001.jpg',
		'/img/sotb-icon.png',
		'/img/sotb-icon.svg',
		'/img/WCskGjtRkeEc81e2shJv_take_photo_ios.jpg',
		'/img/zonderpollen.svg',
		'/img/zonderpollen1.svg',
        'img/icons/android-chrome-192x192.png',
		'/img/icons/android-chrome-192x192.png',
		'/img/icons/android-chrome-512x512.png',
		'/img/icons/apple-touch-icon.png',
		'/img/icons/favicon-16x16.png',
		'/img/icons/favicon-32x32.png',
		'/img/icons/favicon.ico',
		'/img/icons/mstile-70x70.png',
		'/img/icons/mstile-144x144.png',
		'/img/icons/mstile-150x150.png',
		'/img/icons/safari-pinned-tab.svg',
		'/css/ionic.app.css',
        'css/styles.app.css',
        'css/fonts/CalvertMTStd-Bold.ttf',
		'/css/fonts/CalvertMTStd-Bold.woff',
		'/css/fonts/CalvertMTStd-Light.ttf',
		'/css/fonts/CalvertMTStd-Light.woff',
		'/css/fonts/CalvertMTStd.ttf',
		'/css/fonts/CalvertMTStd.woff',
        'lib/ionic/js/ionic.bundle.min.js',
        '/templates/jagenOverzicht.html',
		'/templates/jagenUpload.html',
		'/templates/jagen.html',
		'/templates/test.html',
		'/templates/tellenOverzicht.html',
		'/templates/hommeljacht.html',
		'/templates/tellen.html',
		'/templates/tellenWeer.html',
		'/templates/tellenWind.html',
		'/templates/jagenWind.html',
		'/templates/jagenWeer.html',
		'/templates/kiesEenTaak.html',
		'/templates/welkom.html',
		'/templates/hommelkast.html',
		'/templates/inloggen.html',
		'/templates/menu.html',
		'/templates/foto.html',
		'/templates/telAlleHommels.html',
		'/templates/tellen-aanpassen.html',
		'/templates/signup.html',
		'/templates/opties.html',
		'/templates/fotoOpslaan.html',
		'/templates/alleenGeteld.html'
      ];

var networkFirstFiles = [
  		'/templates/jagenUpload.html',
  		'js/app.js',
		'/js/controllers.js',
		'/js/routes.js',
		'/js/directives.js',
		'/js/services.js',
		'/js/angular-gettext.min.js',
		'/bower_components/webcamjs/webcam.js',
		'/lib/webcam/ng-webcam.js',
		'/lib/upload/ng-file-upload.min.js',
		'/bower_components/ngmap/build/scripts/ng-map.min.js',

];


var cacheFiles = cacheFirstFiles.concat(networkFirstFiles);

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(VERSION).then(cache => {
      return cache.addAll(cacheFiles);
    })
  );
});

self.addEventListener('activate', function(event) {
  var expectedCacheNames = Object.values(VERSION);

  // Active worker won't be treated as activated until promise
  // resolves successfully.
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.map(function(cacheName) {
          if (!expectedCacheNames.includes(cacheName)) {
            console.log('Deleting out of date cache:', cacheName);
            
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});

self.addEventListener('fetch', event => {
  //console.log(event);
  if (event.request.method !== 'GET') { return; }
  if (networkFirstFiles.indexOf(event.request.url) !== -1) {
    console.log('Fetching request from the network');
    event.respondWith(networkElseCache(event));
  } else if (cacheFirstFiles.indexOf(event.request.url) !== -1) {
    console.log('Found response in cache:', response);
    event.respondWith(cacheElseNetwork(event));
  }
  event.respondWith(fetch(event.request));
});

// If cache else network.
// For images and assets that are not critical to be fully up-to-date.
// developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/
// #cache-falling-back-to-network
function cacheElseNetwork (event) {
  return caches.match(event.request).then(response => {
    function fetchAndCache () {
       return fetch(event.request).then(response => {
        // Update cache.
        caches.open(VERSION).then(cache => cache.put(event.request, response.clone()));
        return response;
      });
    }

    // If not exist in cache, fetch.
    if (!response) { return fetchAndCache(); }

    // If exists in cache, return from cache while updating cache in background.
    fetchAndCache();
    return response;
  });
}

// If network else cache.
// For assets we prefer to be up-to-date (i.e., JavaScript file).
function networkElseCache (event) {
  return caches.match(event.request).then(match => {
    if (!match) { return fetch(event.request); }
    return fetch(event.request).then(response => {
      // Update cache.
      caches.open(VERSION).then(cache => cache.put(event.request, response.clone()));
      return response;
    }) || response;
  });
}
